# Simple Schedule System




<img src="https://camo.githubusercontent.com/9c0cc16e1774f7313c159524892e22fdf6954d1ae5ba317ff0c2117c528e45f1/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f2d4d7953514c2d3434373941313f7374796c653d666f722d7468652d6261646765266c6f676f3d6d7973716c266c6f676f436f6c6f723d7768697465" alt="Required Laravel Version" data-canonical-src="https://img.shields.io/badge/Laravel-%E2%89%A5%205.4-ff2d20?style=flat-square&amp;logo=laravel" style="max-width: 100%;">
<img src="https://camo.githubusercontent.com/82712edce51a00c164a9a2efb120f2bd964f50d8c9a4fa2d2ba09dbd71a9551c/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f4c61726176656c2d382d3239326533333f6c6f676f3d4c61726176656c266c6f676f436f6c6f723d666666666666" alt="Required Laravel Version" data-canonical-src="https://img.shields.io/badge/Laravel-%E2%89%A5%205.4-ff2d20?style=flat-square&amp;logo=laravel" style="max-width: 100%;">
<img src="https://camo.githubusercontent.com/20d6366783485558be39b9ed92d5f66f32c357f8a68924ae8f6afa92606d40b2/68747470733a2f2f696d672e736869656c64732e696f2f7061636b61676973742f7068702d762f646f6c656a736b612d64616e69656c2f72696f742d6170692d6c6561677565" alt="Required Laravel Version" data-canonical-src="https://img.shields.io/badge/Laravel-%E2%89%A5%205.4-ff2d20?style=flat-square&amp;logo=laravel" style="max-width: 100%;">

This Simple System Has Two Roles ( Admin and Worker ) , that the Admin can create
Jobs / Tasks and worker can schedule them , after confirming by admin it can be seen on calender

The system is written by laravel 8 and Livewire

#Installation

git clone https://gitlab.com/amiiiink/schedule.git

cd schedule/backend

composer install

After configuring database

php artisan migrate --seed

The demo video is here :

https://youtu.be/i8Bmhi2Lrxg

