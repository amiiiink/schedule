<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class DropAllTables extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'droptables';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      if(App::environment() === "production") {
          exit("I just stopped you getting fired. Love, Amo. \n");
      }
         $arr=DB::select('SHOW TABLES');

//      dd($arr);

         if(!empty($arr)){
             foreach (DB::select('SHOW TABLES') as $table) {
                 $table_array = get_object_vars($table);
                 DB::statement('SET FOREIGN_KEY_CHECKS = 0');
                 \Schema::drop($table_array[key($table_array)]);
                 DB::statement('SET FOREIGN_KEY_CHECKS = 1');
                 echo $table_array[key($table_array)] . " -- table has been dropped !!!! \n ";
             }
         }
         exit("Nothing to Drop!!!!\n");
    }
}
