<?php

namespace App\Console;

use App\Jobs\AppointmentNotify;
use App\Models\Appointment;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function() {
            // check appointments
            $appointments = Appointment::all();

            foreach ($appointments as $app) {
                AppointmentNotify::dispatch($app);
            }

            echo sprintf(
                "In datetime %s exist %d appointment",
                \Carbon\Carbon::now(),
                $appointments->count()
            );
        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
