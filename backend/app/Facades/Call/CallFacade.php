<?php

namespace App\Facades\Call;

use Illuminate\Support\Facades\Facade;

class CallFacade extends Facade {

    public static function getFacadeAccessor()
    {
        return 'call';
    }
}
