<?php

namespace App\Facades\Call;

use App\Models\Message;
use Vonage\Voice\NCCO\NCCO;

class CallService {

    private $client;

    public function __construct()
    {
        $keypair = new \Vonage\Client\Credentials\Keypair(
            file_get_contents(base_path("call.key")),
            env('VONAGE_APLLICATION_ID')
        );

        // create instance for send and receive
        $this->client = new \Vonage\Client($keypair);
    }

    /**
     * Send sms to ... with message ...
     *
     * @param int $mobile
     * @param string $message
     *
     */
    public function sendWithMessage($mobile, $text, $upsheet)
    {
        try {
            $outboundCall = new \Vonage\Voice\OutboundCall(
                new \Vonage\Voice\Endpoint\Phone($mobile),
                new \Vonage\Voice\Endpoint\Phone($mobile)
            );

            $ncco = new NCCO();
            $ncco->addAction(new \Vonage\Voice\NCCO\Action\Talk($text));
            $outboundCall->setNCCO($ncco);
            $this->client->voice()->createOutboundCall($outboundCall);

            $message = Message::create([
                'user_id' => $upsheet->user->id,
                'customer_id' => $upsheet->customer->id,
                'up_sheet_id' => $upsheet->id,
                'result' => true,
                'message' => $text,
                'response_message' => "call sended successfully",
                'status' => 0,
                'to' => $mobile,
                'is_call' => true,
            ]);
        }
        catch(\Exception $err) {
            $mssg = $err->getMessage();
            $message = Message::create([
                'user_id' => $upsheet->user->id,
                'customer_id' => $upsheet->customer->id,
                'up_sheet_id' => $upsheet->id,
                'result' => false,
                'message' => $text,
                'response_message' => $mssg,
                'status' => 99,
                'to' => $mobile,
                'is_call' => true,
            ]);
        }

        return $message;
    }


    /**
     * get SMS messages status
     */
    private function getStatusMessage($status)
    {
        $message = '';
        switch($status) {
            case 'low-balance':
                $message = 'This request could not be performed due to your account balance being low';
                break;
            case 'unauthorized':
                $message = 'You did not provide correct credentials';
                break;
            case 'forbidden':
                $message = 'Your account does not have permission to perform this action';
                break;
            case 'not-found':
                $message = 'The requested resource does not exist, or you do not have access to it';
                break;
            case 'unprovisioned':
                $message = 'The crendentials provided do not have access to the requested product';
                break;
            case 'account-suspended':
                $message = 'This account has been suspended';
                break;
            case 'jwt-expired':
                $message = 'The JWT provided has expired';
                break;
            case 'jwt-revoked':
                $message = 'The JWT provided has been revoked';
                break;
            case 'invalid-api-key':
                $message = 'The API key provided does not exist in our system, or you do not have access';
                break;
            case 'invalid-signature':
                $message = 'The signature provided did not validate.';
                break;
            case 'invalid-ip':
                $message = 'The source IP address of the request is not in our allow list';
                break;
            case 'multiple-auth-methods':
                $message = 'Multiple authentication methods were detected in your request';
                break;
            case 'invalid-id':
                $message = 'The ID provided does not exist in our system';
                break;
            case 'invalid-json':
                $message = 'The request body did not contain valid JSON';
                break;
            case 'wrong-verb':
                $message = 'This endpoint does not support the HTTP verb that you requested';
                break;
            case 'accept-header':
                $message = 'Invalid Accept header provided';
                break;
            case 'content-type-header':
                $message = 'Invalid Content-Type header provided';
                break;
            case 'unavailable-legal':
                $message = 'Unavailable For Legal Reasons';
                break;
            case 'application-suspended':
                $message = 'This application has been suspended';
                break;
        }

        return $message;
    }
}
