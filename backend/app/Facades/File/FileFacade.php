<?php

namespace App\Facades\File;

use Illuminate\Support\Facades\Facade;

class FileFacade extends Facade
{
    public static function getFacadeAccessor()
    {
        return 'fileupload';
    }
}
