<?php

namespace App\Facades\File;

use App\Models\Image;

class FileService
{
    // upload file
    public function upload($file, $ralted_class, $class_id)
    {
        $filename = hash('sha256', $file->getClientOriginalName());
        $file_extension = $file->getClientOriginalExtension();
        $new_name = $filename . '.' . $file_extension;
        $link = url("/uploaded/".$new_name);

        try {
            $target = $file->move(public_path('uploaded/'), $new_name);

            // add to Image Mophy Table
            Image::create([
                "link" => $link,
                "src" => "uploaded/".$new_name,
                "alt" => $new_name,
                "imageable_id" => $class_id,
                "imageable_type" => $ralted_class
            ]);

            return true;
        }
        catch(\Exception $error) {
            return false;
        }
    }

    // delete file

    // update file
}
