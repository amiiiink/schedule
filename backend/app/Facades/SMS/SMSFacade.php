<?php

namespace App\Facades\SMS;

use Illuminate\Support\Facades\Facade;

class SMSFacade extends Facade {

    public static function getFacadeAccessor()
    {
        return 'sms';
    }
}
