<?php

namespace App\Facades\SMS;

use App\Models\Message;

class SMSService {

    private $client;

    public function __construct()
    {
        $basic = new \Vonage\Client\Credentials\Basic(
            config('vonage')["VONAGE_API_KEY"], config('vonage')["VONAGE_API_SECRET"]
        );

        // create instance for send and receive
        $this->client = new \Vonage\Client($basic);
    }

    /**
     * Send sms to ... with message ...
     *
     * @param $data
     * @return bool
     */
    public function sendWithMessage($data): bool
    {
        $response = $this->client->sms->send(
            new \Vonage\SMS\Message\SMS(strval($data['mobile']), '18334881502', $data['text'])
        );

        $message = $response->current();

        Message::create([
            'user_id' => $data['user_id'],
            'customer_id' => $data['customer_id'],
            'up_sheet_id' => $data['upsheet_id'],
            'result' => intval($message->getStatus()) == 0 ? true : false,
            'message' => $data['text'],
            'response_message' => $this->getStatusMessage($message->getStatus()),
            'status' => $message->getStatus(),
            'to' => $message->getTo(),
        ]);

        if ($message->getStatus() == 0) {
            return true;
        }

        return false;
    }


    /**
     * get SMS messages status
     */
    private function getStatusMessage($status)
    {
        $message = '';
        switch(intval($status)) {
            case 0:
                $message = 'Success - The message was successfully accepted for delivery.';
                break;
            case 1:
                $message = 'Throttled - You are sending SMS faster than the account limit';
                break;
            case 2:
                $message = 'Missing Parameters - Your request is missing one of the required parameters: from, to, api_key, api_secret or text.';
                break;
            case 3:
                $message = 'Invalid Parameters - The value of one or more parameters is invalid.';
                break;
            case 4:
                $message = 'Invalid Credentials	- Your API key and/or secret are incorrect, invalid or disabled.';
                break;
            case 5:
                $message = 'Internal Error - An error has occurred in the platform whilst processing this message.';
                break;
            case 6:
                $message = 'Invalid Message - The platform was unable to process this message, for example, an unrecognized number prefix.';
                break;
            case 7:
                $message = 'Number Barred - The number you are trying to send messages to is on the list of barred numbers.';
                break;
            case 8:
                $message = 'Partner Account Barred - Your Vonage account has been suspended. Contact support@nexmo.com.';
                break;
            case 9:
                $message = 'Partner Quota Violation - ou do not have sufficient credit to send the message. Top-up and retry.';
                break;
            case 10:
                $message = 'Too Many Existing Binds	- The number of simultaneous connections to the platform exceeds your account allocation.';
                break;
            case 11:
                $message = 'Account Not Enabled For HTTP - This account is not provisioned for the SMS API, you should use SMPP instead.';
                break;
            case 12:
                $message = 'Message Too Long - The message length exceeds the maximum allowed.';
                break;
            case 14:
                $message = 'Invalid Signature - The signature supplied could not be verified.';
                break;
            case 15:
                $message = 'Invalid Sender Address - You are using a non-authorized sender ID in the from field. This is most commonly in North America, where a Vonage long virtual number or short code is required.';
                break;
            case 22:
                $message = 'Invalid Network Code - The network code supplied was either not recognized, or does not match the country of the destination address.';
                break;
            case 23:
                $message = 'Invalid Callback URL - The callback URL supplied was either too long or contained illegal characters.';
                break;
            case 29:
                $message = 'Non-Whitelisted Destination - Your Vonage account is still in demo mode. While in demo mode you must add target numbers to your whitelisted destination list. Top-up your account to remove this limitation.';
                break;
            case 32:
                $message = 'Signature And API Secret Disallowed - A signed request may not also present an api_secret';
                break;
            case 33:
                $message = 'Number De-activated - The number you are trying to send messages to is de-activated and may not receive them.';
                break;
        }

        return $message;
    }
}
