<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Customer;
use App\Models\Email;
use App\Models\Message;
use App\Models\Schedule;
use App\Models\Status;
use App\Models\UpSheet;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $current_user = auth()->user();

        if (! $current_user->is_admin()) {
            $appointments = Appointment::where('user_id', $current_user->id)
                            ->whereDate("created_at", \Carbon\Carbon::today())
                            ->orderBy("schedule", "ASC")
                            ->get();

            return view('dashboard');
        }



        return view('dashboard');


    }

    public function sellerDashboardInformation($sellers, $id)
    {
        $user = User::where('role_id', User::USER_SALER)
            ->where('id', $id)->first();

        $appointments = Appointment::where('user_id', $user->id)
                ->whereDate("created_at", \Carbon\Carbon::today())
                ->orderBy("schedule", "ASC")
                ->get();

        $customers_count = Customer::where('user_id', $user->id)->count();
        $upsheetsCreated = UpSheet::where('user_id', $user->id)->count();
        $upsheetSolded = UpSheet::where('user_id', $user->id)->where('status_id', Status::SOLD)->count();
        $upsheetClosed = UpSheet::where('user_id', $user->id)->where('status_id', Status::CLOSING)->count();
        $upsheetLost = UpSheet::where('user_id', $user->id)->where('status_id', Status::LOST)->count();
        $upsheetDeliverd = UpSheet::where('user_id', $user->id)->where('status_id', Status::DELIVERED)->count();
        $percentage_closing = ($upsheetsCreated == 0) ? 0 : intval(ceil(($upsheetClosed * 100) / $upsheetsCreated));

        // quey filter base on dates
        $today_upsheets = UpSheet::where('user_id', $user->id)->whereDate("created_at", \Carbon\Carbon::today())
                    ->count();
        $yesterday_upsheets = UpSheet::where('user_id', $user->id)->whereDate("created_at", \Carbon\Carbon::yesterday())
                    ->count();
        $current_month_upsheets = UpSheet::where('user_id', $user->id)->whereMonth("created_at", \Carbon\Carbon::today()->month)
                    ->count();
        # active
        $active_today_upsheets = UpSheet::where('user_id', $user->id)->where("status_id", Status::ACTIVE)->whereDate("created_at", \Carbon\Carbon::today())
                    ->count();
        $active_yesterday_upsheets = UpSheet::where('user_id', $user->id)->where("status_id", Status::ACTIVE)->whereDate("created_at", \Carbon\Carbon::yesterday())
                    ->count();
        $active_current_month_upsheets = UpSheet::where('user_id', $user->id)->where("status_id", Status::ACTIVE)->whereMonth("created_at", \Carbon\Carbon::today()->month)
                    ->count();
        # lost
        $lost_today_upsheets = UpSheet::where('user_id', $user->id)->where("status_id", Status::LOST)->whereDate("created_at", \Carbon\Carbon::today())
                    ->count();
        $lost_yesterday_upsheets = UpSheet::where('user_id', $user->id)->where("status_id", Status::LOST)->whereDate("created_at", \Carbon\Carbon::yesterday())
                    ->count();
        $lost_current_month_upsheets = UpSheet::where('user_id', $user->id)->where("status_id", Status::LOST)->whereMonth("created_at", \Carbon\Carbon::today()->month)
                    ->count();
        # e-leads
        $eleads_today_upsheets = Customer::where('user_id', $user->id)->whereDate("created_at", \Carbon\Carbon::today())
                    ->count();
        $eleads_yesterday_upsheets = Customer::where('user_id', $user->id)->whereDate("created_at", \Carbon\Carbon::yesterday())
                    ->count();
        $eleads_current_month_upsheets = Customer::where('user_id', $user->id)->whereMonth("created_at", \Carbon\Carbon::today()->month)
                    ->count();
        # Sold
        $sold_today_upsheets = UpSheet::where('user_id', $user->id)->where("status_id", Status::SOLD)->whereDate("created_at", \Carbon\Carbon::today())
                    ->count();
        $sold_yesterday_upsheets = UpSheet::where('user_id', $user->id)->where("status_id", Status::SOLD)->whereDate("created_at", \Carbon\Carbon::yesterday())
                    ->count();
        $sold_current_month_upsheets = UpSheet::where('user_id', $user->id)->where("status_id", Status::SOLD)->whereMonth("created_at", \Carbon\Carbon::today()->month)
                    ->count();
        # Delivered
        $delivered_today_upsheets = UpSheet::where('user_id', $user->id)->where("status_id", Status::DELIVERED)->whereDate("created_at", \Carbon\Carbon::today())
                    ->count();
        $delivered_yesterday_upsheets = UpSheet::where('user_id', $user->id)->where("status_id", Status::DELIVERED)->whereDate("created_at", \Carbon\Carbon::yesterday())
                    ->count();
        $delivered_current_month_upsheets = UpSheet::where('user_id', $user->id)->where("status_id", Status::DELIVERED)->whereMonth("created_at", \Carbon\Carbon::today()->month)
                    ->count();
        # Delivered
        $closing_today_upsheets = UpSheet::where('user_id', $user->id)->where("status_id", Status::CLOSING)->whereDate("created_at", \Carbon\Carbon::today())
                    ->count();
        $closing_yesterday_upsheets = UpSheet::where('user_id', $user->id)->where("status_id", Status::CLOSING)->whereDate("created_at", \Carbon\Carbon::yesterday())
                    ->count();
        $closing_current_month_upsheets = UpSheet::where('user_id', $user->id)->where("status_id", Status::CLOSING)->whereMonth("created_at", \Carbon\Carbon::today()->month)
                    ->count();


        # table email, call, ...
        $today_emails = Email::where('user_id', $user->id)->whereDate("created_at", \Carbon\Carbon::today())->count();
        $yesterday_emails = Email::where('user_id', $user->id)->whereDate("created_at", \Carbon\Carbon::yesterday())->count();
        $month_emails = Email::where('user_id', $user->id)->whereMonth("created_at", \Carbon\Carbon::today()->month)->count();
        # sms
        $today_sms = Message::where('user_id', $user->id)->where("is_call", false)->whereDate("created_at", \Carbon\Carbon::today())->count();
        $yesterday_sms = Message::where('user_id', $user->id)->where("is_call", false)->whereDate("created_at", \Carbon\Carbon::yesterday())->count();
        $month_sms = Message::where('user_id', $user->id)->where("is_call", false)->whereMonth("created_at", \Carbon\Carbon::today()->month)->count();
        # call
        $today_call = Message::where('user_id', $user->id)->where("is_call", true)->whereDate("created_at", \Carbon\Carbon::today())->count();
        $yesterday_call = Message::where('user_id', $user->id)->where("is_call", true)->whereDate("created_at", \Carbon\Carbon::yesterday())->count();
        $month_call = Message::where('user_id', $user->id)->where("is_call", true)->whereMonth("created_at", \Carbon\Carbon::today()->month)->count();

        # schedule
        $today_schedules = Schedule::where('user_id', $user->id)->whereDate("created_at", \Carbon\Carbon::today())->count();
        $yesterday_schedules = Schedule::where('user_id', $user->id)->whereDate("created_at", \Carbon\Carbon::yesterday())->count();
        $month_schedules = Schedule::where('user_id', $user->id)->whereMonth("created_at", \Carbon\Carbon::today()->month)->count();
        # appointment
        $today_appointment = Appointment::where('user_id', $user->id)->whereDate("created_at", \Carbon\Carbon::today())->count();
        $yesterday_appointment = Appointment::where('user_id', $user->id)->whereDate("created_at", \Carbon\Carbon::yesterday())->count();
        $month_appointment = Appointment::where('user_id', $user->id)->whereMonth("created_at", \Carbon\Carbon::today()->month)->count();

        $data = compact(
            'appointments', 'sellers', 'upsheetsCreated',
            'upsheetSolded', 'percentage_closing', 'upsheetLost',
            'upsheetDeliverd', 'customers_count', 'today_upsheets',
            'yesterday_upsheets', 'current_month_upsheets',
            'active_today_upsheets',
            'active_yesterday_upsheets',
            'active_current_month_upsheets',
            'lost_today_upsheets',
            'lost_yesterday_upsheets',
            'lost_current_month_upsheets',
            'eleads_today_upsheets',
            'eleads_yesterday_upsheets',
            'eleads_current_month_upsheets',
            'sold_today_upsheets',
            'sold_yesterday_upsheets',
            'sold_current_month_upsheets',
            'delivered_today_upsheets',
            'delivered_yesterday_upsheets',
            'delivered_current_month_upsheets',
            'closing_today_upsheets',
            'closing_yesterday_upsheets',
            'closing_current_month_upsheets',
            'today_emails',
            'yesterday_emails',
            'month_emails',
            'today_sms',
            'yesterday_sms',
            'month_sms',
            'today_call',
            'yesterday_call',
            'month_call',
            'today_schedules',
            'yesterday_schedules',
            'month_schedules',
            'today_appointment',
            'yesterday_appointment',
            'month_appointment', 'id'
        );

        return $data;
    }

    public function readAllNotification()
    {
        $user = auth()->user();
        foreach ($user->unreadNotifications as $notification) {
            $notification->markAsRead();
        }

        return redirect()->back();
    }

    public function readSingleNotification($id)
    {
        $user = auth()->user();
        $notification = $user->unreadNotifications->where("id", $id)->first();
        return view("dashboard.notification.single", compact('notification'));
    }

    public function markAsReadNotification($id)
    {
        $user = auth()->user();
        $user->unreadNotifications->where("id", $id)->markAsRead();

        return redirect()->back();
    }

    public function showAllMessage()
    {
        $messages = Message::orderBy('created_at', 'asc')->paginate(10);
        return view("dashboard.messages", compact('messages'));
    }

    public function accept(Request $request)
    {
//        dd($request->all());
        $updateStatus= DB::table('schedules')
            ->where('id',$request->id)
            ->update(array(
                'reminder'=>$request->reminder,
                'status'=>1,
                'updated_at'=>Carbon::now()
            ));
        session()->flash("success", "Schedule successfuly accepted");
        return redirect()->back();
    }
}
