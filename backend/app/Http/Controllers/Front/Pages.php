<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\PaidAds;
use Illuminate\Http\Request;

class Pages extends Controller
{
    /**
     * Main Page
     */
    public function index()
    {
        return view('welcome');
    }

    public function adPanel()
    {
        $ads = PaidAds::orderBy("id", "DESC")->paginate(10);
        return view("dashboard.ads.index", compact('ads'));
    }

    public function adPanelSingle($id)
    {
        $ad = PaidAds::find($id);
        $data = json_decode($ad->questions);
        return view("dashboard.ads.show", compact('data'));
    }

    public function adsPaid()
    {
        return view('front.paid-ads');
    }

    public function success()
    {
        return view('front.success');
    }

    public function error()
    {
        return view('front.error');
    }

    public function adsPaidSubmit(Request $request)
    {
        $request->validate([
            "social" => "required",
            "achieve" => "required",
            "vehicle_type" => "required",
            "finance_type" => "required",
            "target_customer" => "required",
            "location" => "required",
            "vehicle_sell" => "required",
            "language" => "required",
            "daily_budget" => "required",
        ]);

        $data = [
            "social" => $request->social,
            "social_extra" => $request->social_extra,
            "achieve" => $request->achieve,
            "achieve_extra" => $request->achieve_extra,
            "vehicle_type" => $request->vehicle_type,
            "vehicle_type_extra" => $request->vehicle_type_extra,
            "finance_type" => $request->finance_type,
            "finance_type_extra" => $request->finance_type_extra,
            "target_customer" => $request->target_customer,
            "target_customer_extra" => $request->target_customer_extra,
            "location" => $request->location,
            "location_extra" => $request->location_extra,
            "vehicle_sell" => $request->vehicle_sell,
            "vehicle_sell_extra" => $request->vehicle_sell_extra,
            "language" => $request->language,
            "language_extra" => $request->language_extra,
            "daily_budget" => $request->daily_budget,
            "daily_budget_extra" => $request->daily_budget_extra,
            "extra_comment" => $request->extra_comment
        ];

        $data_json_format = json_encode($data);

        $ads = PaidAds::create([
            'questions' => $data_json_format
        ]);

        if ($ads) {
            return redirect(route('success-message'));
        }

        return redirect(route('error-message'));
    }
}
