<?php

namespace App\Http\Controllers\Roles;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role as PermissionRole;

class PermissonController extends Controller
{
    public function index()
    {
        if (! auth()->user()->is_admin()) {
            return abort(404);
        }

        $users = User::paginate(8);
        $roles = PermissionRole::paginate(8);
        $permissions = Permission::all();
        return view("dashboard.permission.index", compact('users', 'permissions', 'roles'));
    }

    public function create()
    {
        return view('dashboard.permission.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
        ]);

        $newPermission = Permission::create([
            'name' => $request->name,
            'guard_name' => 'web',
        ]);

        if ($newPermission) {
            session()->flush('success', 'Created New Permission');
            return redirect(route('permission.index'));
        }

        session()->flush('success', 'Faild to proccess');
        return redirect(route('permission.index'));
    }

    public function assign()
    {
        $users = User::all();
        $permissions = Permission::all();
        return view('dashboard.permission.assign', compact('users', 'permissions'));
    }

    public function assignStore(Request $request)
    {
        $request->validate([
            "user" => 'required',
            'permissions' => 'required|array'
        ]);

        $user = User::find($request->user);
        $user->givePermissionTo($request->permissions);

        session()->flush('success', 'New Permission Assigned');
        return redirect(route('permission.index'));
    }

    public function assignRole()
    {
        $roles = Role::all();
        $permissions = Permission::all();

        return view('dashboard.permission.assign-role', compact('roles','permissions'));
    }

    public function assignRoleStore(Request $request)
    {
        $request->validate([
            "role" => 'required',
            'permissions' => 'required|array'
        ]);

        $role = PermissionRole::find($request->role);
        $role->syncPermissions($request->permissions);

        session()->flush('success', 'New Permission Assigned');
        return redirect(route('permission.index'));
    }
}
