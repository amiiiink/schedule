<?php

namespace App\Http\Controllers\Tasks;

use App\Http\Controllers\Controller;
use App\Models\SalesTask;
use App\Models\User;
use Illuminate\Http\Request;

class SalesTaskController extends Controller
{
    public function index()
    {
        $users = User::where("role_id", User::USER_SALER)->paginate("10");
        return view("dashboard.tasks.index", compact('users'));
    }

    public function show($id)
    {
        $tasks = SalesTask::where('user_id', $id)->get();
        return view("dashboard.tasks.show", compact('tasks'));
    }

    public function create()
    {
        $users = User::where("role_id", User::USER_SALER)->get();
        return view("dashboard.tasks.create", compact('users'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required|integer',
            'text' => 'required|string'
        ]);

        $new_task = SalesTask::create([
            'admin_id' => auth()->user()->id,
            'user_id' => $request->user_id,
            'task' => $request->text,
            'is_done' => false,
        ]);

        if ($new_task) {
            return redirect(route('tasklist.index'))->with('success', "new task assigned");
        }

        return redirect(route('tasklist.create'))->with('error', "error to create new task");
    }

    public function statusDone(Request $request)
    {
        $request->validate([
            "confirm" => "required|boolean",
            "task" => "required"
        ]);

        $task = SalesTask::find($request->task);
        $task->is_done = 1;
        $task->save();

        return redirect()->back();
    }

    public function statusConfirm(Request $request)
    {
        $request->validate([
            "confirm" => "required|boolean",
            "task" => "required"
        ]);

        $task = SalesTask::find($request->task);
        $task->confirm = 1;
        $task->save();

        return redirect()->back();
    }

    public function statusReject(Request $request)
    {
        $request->validate([
            "confirm" => "required|boolean",
            "task" => "required"
        ]);

        $task = SalesTask::find($request->task);
        $task->confirm = 0;
        $task->is_done = 0;
        $task->save();

        return redirect()->back();
    }
}
