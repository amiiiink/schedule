<?php

namespace App\Http\Controllers;

use App\Facades\File\FileFacade;
use App\Models\UpSheet;
use App\Traits\ActivityTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UploadFileController extends Controller
{
    use ActivityTrait;
    public function create(Request $request, $id)
    {
        $request->validate([
            "file" => 'required|max:10000|mimes:png,jpg,jpeg,pdf,xsl,csv'
        ]);

        $upsheet = UpSheet::findOrFail($id);

        if ($request->hasFile('file')) {
            $response = FileFacade::upload($request->file('file'), UpSheet::class, $upsheet->id);
            if ($response) {

                # add activity trait
                $this->addActivity(
                    "User %s Upload New File ",
                    auth()->user()->firstname,
                    auth()->user()->lastname,
                    null,
                    null,
                    auth()->user()->id,
                    $upsheet->id,
                    null
                );

                session()->flash('success', 'File Uploaded');
                return redirect()->back();
            }
        }

        session()->flash('error', 'Problem to upload file');
        return redirect()->back();
    }

    public function delete()
    {

    }

    public function edit()
    {

    }


}
