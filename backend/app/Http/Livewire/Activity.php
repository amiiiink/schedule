<?php

namespace App\Http\Livewire;

use App\Models\UpSheet;
use App\Models\User;
use Livewire\Component;

class Activity extends Component
{
    public function render()
    {
        // auth user
        $current_user = auth()->user();

        // check user is not admin
        if (!$current_user->is_admin()) {
            // saler upsheets
            $upsheets = UpSheet::where('user_id', $current_user->id)
                ->orderBy('created_at', 'DESC')
                ->paginate(15);

            return view('livewire.activity', compact('upsheets'))
                ->layout('layouts.panel');
        }

        // admin all upsheets
        $upsheets = UpSheet::orderBy('created_at', 'DESC')->paginate(15);
        return view('livewire.activity', compact('upsheets'))
            ->layout('layouts.panel');
    }
}
