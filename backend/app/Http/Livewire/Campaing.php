<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Campaing extends Component
{
    public function render()
    {
        return view('livewire.campaing')
            ->layout('layouts.panel');
    }
}
