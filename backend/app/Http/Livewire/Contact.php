<?php

namespace App\Http\Livewire;

use App\Models\Customer;
use Livewire\Component;

class Contact extends Component
{
    public function render()
    {
        // auth user
        $currentUser = auth()->user();

        // is admin
        if ($currentUser->is_admin()) {
            $customers = Customer::orderBy('created_at', 'DESC')->paginate(10);
            return view('livewire.contact', compact('customers'))
            ->layout('layouts.panel');
        }

        $customers = Customer::where('user_id', $currentUser->id)
            ->orderBy('created_at', 'DESC')
            ->paginate(2);
        return view('livewire.contact', compact('customers'))
            ->layout('layouts.panel');
    }
}
