<?php

namespace App\Http\Livewire\Forms;


use App\Models\Activity;
use App\Models\myJob;
use App\Models\Status;


use Livewire\Component;

class AppointmentForm extends Component
{

    public $upsheet_id = 0;
    public $customer_id = 0;
    public $schedule = '';

    public function mount($upsheetId, $customerId)
    {
        $this->upsheet_id = $upsheetId;
        $this->customer_id = $customerId;
    }

    protected $rules = [
        'schedule' => 'required|min:5'
    ];

    public function addAppointment()
    {
        $this->validate();



        $upsheet = myJob::find($this->upsheet_id);


        $newSchedule = \App\Models\Schedule::create([
            'user_id' => auth()->user()->id,
            'up_sheet_id' =>$this->upsheet_id,
            'reminder' => 2,
            'message' => "dsasda",
        ]);



        session()->flash("success", "Appointment successfuly created");
        return redirect(
            route('upsheets.show', $this->upsheet_id)
        );
    }

    public function render()
    {
        return view('livewire.forms.appointment-form');
    }
}
