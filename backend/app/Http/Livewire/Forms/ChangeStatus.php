<?php

namespace App\Http\Livewire\Forms;

use App\Models\Status;
use App\Models\UpSheet;
use Livewire\Component;

class ChangeStatus extends Component
{
    public $upsheetId;
    public $user;

    public $statusId;


    public function mount($upsheetId)
    {
        $this->upsheetId = $upsheetId;
        $this->statusId = UpSheet::find($upsheetId)->status->id;
        // $user = auth()->user();
    }

    protected $rules = [
        "statusId" => 'required|integer',
    ];

    public function changeUpsheetStatus()
    {
        // validated form
        $this->validate();

        $upsheet = Upsheet::find($this->upsheetId);
        $upsheet->update([
            "status_id" => $this->statusId
        ]);

        // redirect back to upsheet
        session()->flash("message", "Status of upsheet changed.");
        return redirect(route('upsheets.show', $this->upsheetId));
    }

    public function changeStatus()
    {
        $this->emit('changeUpsheetStatus');
    }

    public function render()
    {
        $upsheet = UpSheet::find($this->upsheetId);
        $status = Status::all();
        return view('livewire.forms.change-status', compact('status', 'upsheet'));
    }
}
