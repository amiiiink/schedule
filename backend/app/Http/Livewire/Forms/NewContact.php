<?php

namespace App\Http\Livewire\Forms;

use App\Models\Customer;
use App\Models\Role;
use App\Notifications\RegisterContactNotification;
use Livewire\Component;

class NewContact extends Component
{
    public $firstname;
    public $lastname;
    public $email;
    public $mobile;

    protected $rules = [
        "firstname" => 'required|string|max:45',
        "lastname" => 'required|string|max:45',
        "mobile" => 'required_if:email,null|unique:users',
        "email" => 'required_if:mobile,null|unique:users',
    ];

    public function render()
    {
        return view('livewire.forms.new-contact')
        ->layout('layouts.panel');
    }

    public function saveContact()
    {
        // validated form
        $this->validate();

        if (!is_null($this->mobile)) {
            $this->validate([
                "mobile" => "regex:/^\([0-9]{3}\)[0-9]{3}-[0-9]{4}$/i"
            ]);
            $this->mobile = str_replace(['(', ')', '-'], '', $this->mobile);
            $this->mobile = '1' . strval($this->mobile);
        }

        $mobile = is_null($this->mobile) ? '' : '1'.$this->mobile;
        $email = is_null($this->email) ? '' : $this->email;

        // search customer
        $customer = Customer::where("mobile", $mobile)
                    ->orWhere('email', $email)->first();

        // add new customer
        if (is_null($customer)) {
            $customer = Customer::create([
                "user_id" => auth()->user()->id,
                "firstname" => $this->firstname,
                "lastname" => $this->lastname,
                "email" => $this->email,
                "mobile" => $this->mobile,
            ]);

            // send notification
            auth()->user()->notify(new RegisterContactNotification($customer));
            Role::find(Role::ADMIN)->notify(new RegisterContactNotification($customer));

            // send alarm success or error
            session()->flash('success', 'New Contact Created.');
            return redirect()->to(route('panel.contacts'));
        }

        // return to page upsheets
        session()->flash('message', 'Contact already exist.');
        return redirect()->to(route('panel.contacts'));
    }
}
