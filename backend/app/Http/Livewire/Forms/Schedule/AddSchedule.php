<?php

namespace App\Http\Livewire\Forms\Schedule;

use App\Models\Activity;
use App\Models\Schedule;
use Livewire\Component;

class AddSchedule extends Component
{

    public $userId;
    public $upsheetId;

    public $message = '';
    public $reminder = '';

    protected $rules = [
        "message" => 'required|string',
        'reminder' => 'required',
    ];

    public function mount($upsheetId, $userId)
    {
        $this->upsheetId = $upsheetId;
        $this->userId = $userId;
    }

    public function addScheduleForUpsheet()
    {
        $this->validate();

        $newSchedule = Schedule::create([
            'user_id' => $this->userId,
            'up_sheet_id' => $this->upsheetId,
            'reminder' => $this->reminder,
            'message' => $this->message,
        ]);

        if ($newSchedule) {
            session()->flash("success", "Schedule successfuly added");
            return redirect(route('upsheets.show', $this->upsheetId));
        }

        session()->flash("error", "faild to add schedule");
        return redirect(route('upsheets.show', $this->upsheetId));
    }

    public function render()
    {
        return view('livewire.forms.schedule.add-schedule');
    }
}
