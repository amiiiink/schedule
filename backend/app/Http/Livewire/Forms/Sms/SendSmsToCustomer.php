<?php

namespace App\Http\Livewire\Forms\Sms;

use App\Facades\Call\CallFacade;
use App\Facades\SMS\SMSFacade;
use App\Models\Activity;
use App\Models\UpSheet;
use App\Traits\ActivityTrait;
use Carbon\Carbon;
use Livewire\Component;

class SendSmsToCustomer extends Component
{
    use ActivityTrait;
    public $userId = 0;
    public $upsheetId = 0;
    public $message = '';
    public $type;

    protected $rules = [
        'message' => 'required|string|min:3|max:60',
        'type' => 'required'
    ];

    public function mount($userId, $upsheetId)
    {
        $this->userId = $userId;
        $this->upsheetId = $upsheetId;
    }

    public function sendMessageToCustomer()
    {
        $this->validate();
        $upsheet = UpSheet::find($this->upsheetId);

        $customer = $upsheet->customer;
        $customerNumber = $customer->mobile;


        if (! is_null($customerNumber)) {

            $send_sms_data = [
                'mobile' => $customerNumber,
                'text' => $this->message,
                'user_id' => $this->userId,
                'customer_id' => $customer->id,
                'upsheet_id' => $this->upsheetId
            ];


            if ($this->type == "call") {
                $response = CallFacade::sendWithMessage($customerNumber, $this->message, $upsheet);
                # add activity trait
                $this->addActivity(
                    "User %s Send New Call To Customer %s",
                    auth()->user()->firstname,
                    auth()->user()->lastname,
                    $customer->firstname,
                    $customer->lastname,
                    $this->userId,
                    $this->upsheetId,
                    null
                );


            } else {


                $response = SMSFacade::sendWithMessage(
                    $send_sms_data
                );


                # add activity trait
                $this->addActivity(
                    "User %s Send New SMS To Customer %s",
                    auth()->user()->firstname,
                    auth()->user()->lastname,
                    $customer->firstname,
                    $customer->lastname,
                    $this->userId,
                    $this->upsheetId,
                    null
                );

            }

            if ($response) {
                // save sms/call to upsheet
                session()->flash("success", "Action Sended");
                return redirect(
                    route('upsheets.show', $this->upsheetId)
                );
            }

            // sms/call not sended or rejected
            session()->flash("error", "Action faild to send");
            return redirect(
                route('upsheets.show', $this->upsheetId)
            );
        }else{
            session()->flash("error", "Customer Does Not Have Phone Number");
        }
    }

    public function render()
    {
        return view('livewire.forms.sms.send-sms-to-customer');
    }
}
