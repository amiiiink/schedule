<?php
namespace App\Http\Livewire\Forms\Tasks;

use App\Models\Activity;
use App\Models\UserTask;
use App\Traits\ActivityTrait;
use Livewire\Component;

class AddTask extends Component
{
    use ActivityTrait;
    public $user_id;
    public $upsheet_id;
    public $reminder = '';
    public $message = '';

    protected $rules = [
        'reminder' => 'required',
        'message' => 'required|string'
    ];

    public function mount($userId, $upsheetId)
    {
        $this->user_id = $userId;
        $this->upsheet_id = $upsheetId;
    }

    public function store_task()
    {
        $this->validate();

        $new_task = UserTask::create([
            "user_id" => $this->user_id,
            "up_sheet_id" => $this->upsheet_id,
            "reminder" => $this->reminder,
            "message" => $this->message,
        ]);

        # add activity trait
        $this->addActivity(
            "User %s Create New Task",
            auth()->user()->firstname,
            auth()->user()->lastname,
            null,
            null,
            $this->user_id,
            $this->upsheet_id,
            $new_task->created_at
        );


        if ($new_task) {
            session()->flash("success", "created new task");
            return redirect(
                route('upsheets.show', $this->upsheet_id)
            );
        }

        session()->flash("error", "Can't create task call provider");
        return redirect(
            route('upsheets.show', $this->upsheet_id)
        );
    }

    public function render()
    {
        return view('livewire.forms.tasks.add-task');
    }
}
