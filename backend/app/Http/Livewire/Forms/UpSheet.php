<?php

namespace App\Http\Livewire\Forms;

use App\Models\myJob;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

use App\Models\Customer;
use App\Models\Role;
use App\Models\Status;
use App\Models\Temperature;
use App\Models\UpSheet as ModelsUpSheet;
use App\Notifications\RegisterNewUpSheetNotification;

class UpSheet extends Component
{
    public $title;
    public $description;


    protected $rules = [
        "title" => 'required|string|max:45',
        "description" => 'required|string|max:45',
    ];

    public function saveUpsheet()
    {

        // validated form
        $this->validate();



        // Add New Job
        $newUpsheet = myJob::create([
            "title" => $this->title,
            "description" => $this->description,
        ]);



        // send alarm success or error
        session()->flash('success', 'Job successfully created.');

        // return to page upsheets
        return redirect()->to(route('panel.upsheets'));
    }

    public function saveNewUpsheet()
    {
        // validated form
        $this->validate();

        // fetch user if exist
        if (!is_null($this->email) && is_null($this->mobile)) {
            $customer = Customer::where('email', $this->email)->first();
        }
        else if (!is_null($this->mobile) && is_null($this->email)) {
            $customer = Customer::where("mobile", $this->mobile)->first();
        }
        else {
            $customer = Customer::where("mobile", $this->mobile)
                ->orWhere('email', $this->email)->first();
        }

        // add new upsheet
        $newUpsheet = ModelsUpSheet::create([
            "user_id" => Auth::user()->id,
            "customer_id" => $customer->id,
            "status_id" => Status::ACTIVE,
            "temperature_id" => Temperature::HOT
        ]);


        // send notification
        auth()->user()->notify(new RegisterNewUpSheetNotification($newUpsheet));
        Role::find(Role::ADMIN)->notify(new RegisterNewUpSheetNotification($newUpsheet));

        // send alarm success or error
        session()->flash('success', 'UpSheet successfully created.');

        // return to page upsheets
        return redirect()->to(route('panel.upsheets'));
    }

    public function render()
    {
        return view('livewire.forms.up-sheet')
        ->layout('layouts.panel');
    }
}
