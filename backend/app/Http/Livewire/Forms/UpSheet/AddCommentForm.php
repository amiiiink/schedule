<?php

namespace App\Http\Livewire\Forms\UpSheet;

use App\Facades\SMS\SMSFacade;
use App\Models\Activity;
use App\Models\Comment;
use App\Models\UpSheet;
use App\Traits\ActivityTrait;
use Livewire\Component;

class AddCommentForm extends Component
{
    use ActivityTrait;
    public $upsheetId;
    public $userId;

    public $message = '';

    public function mount($upsheetId, $userId)
    {
        $this->upsheetId = $upsheetId;
        $this->userId = $userId;
    }

    protected $rules = [
        'message' => 'required|string|max:180'
    ];

    public function addNewCommentForUpsheet()
    {
        $this->validate();

        $comment = Comment::create([
            "up_sheet_id" => $this->upsheetId,
            "user_id" => $this->userId,
            "message" => $this->message,
        ]);

        # add activity trait
        $this->addActivity(
            "User %s Add New Note",
            auth()->user()->firstname,
            auth()->user()->lastname,
            null,
            null,
            $this->userId,
            $this->upsheetId,
            $comment->created_at
        );


        if ($comment) {
            session()->flash("success", "new message added");
            return redirect(
                route('upsheets.show', $this->upsheetId)
            );
        }

        session()->flash("error", "call provider error happend");
        return redirect(
            route('upsheets.show', $this->upsheetId)
        );
    }

    public function render()
    {
        return view('livewire.forms.up-sheet.add-comment-form');
    }
}
