<?php

namespace App\Http\Livewire\Forms\UpSheet;

use App\Models\Activity;
use App\Models\UpSheet;
use App\Models\User;
use App\Traits\ActivityTrait;
use Carbon\Carbon;
use Livewire\Component;

class ChangeOwnerForm extends Component
{
    use ActivityTrait;

    public $upsheetId;
    public $users;
    public $userId = 'none';

    protected $rules = [
        'userId' => 'required|numeric'
    ];

    protected $messages = [
        "userId.numeric" => "Please select a user"
    ];

    public function mount($upsheetId)
    {
        $this->users = User::all();
        $this->$upsheetId = $upsheetId;
    }

    public function updataUpsheetOwner()
    {
        $upsheet = UpSheet::find($this->upsheetId);
        $upsheet->user_id = intval($this->userId);
        $upsheet->save();

        # add activity trait
        $this->addActivity(
            "User %s Change Owner Of Upsheet",
            auth()->user()->firstname,
            auth()->user()->lastname,
            null,
            null,
            auth()->user()->id,
            $this->upsheetId,
            null
        );


        session()->flash("success", "Owner of this Upsheet changed");
        return redirect(
            route('upsheets.show', $this->upsheetId)
        );
    }

    public function render()
    {
        return view('livewire.forms.up-sheet.change-owner-form');
    }
}
