<?php

namespace App\Http\Livewire\Forms\UpSheet;

use App\Models\Comment;
use Livewire\Component;

class DeleteCommentForm extends Component
{
    public $commentId;
    public $upsheetId;

    public function mount($commentId, $upsheetId)
    {
        $this->commentId = $commentId;
        $this->upsheetId = $upsheetId;
    }

    public function DeleteUpSheetComment()
    {
        $deleted = Comment::find($this->commentId)->delete();
        if($deleted)
        {
            session()->flash("success", "Message Deleted");
            return redirect(
                route('upsheets.show', $this->upsheetId)
            );
        }

        session()->flash("error", "Error for delete message");
        return redirect(
            route('upsheets.show', $this->upsheetId)
        );
    }

    public function render()
    {
        return view('livewire.forms.up-sheet.delete-comment-form');
    }
}
