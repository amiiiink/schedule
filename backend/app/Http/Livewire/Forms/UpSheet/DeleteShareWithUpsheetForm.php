<?php

namespace App\Http\Livewire\Forms\UpSheet;

use App\Models\Activity;
use App\Models\UpSheet;
use App\Traits\ActivityTrait;
use Carbon\Carbon;
use Livewire\Component;

class DeleteShareWithUpsheetForm extends Component
{
    use ActivityTrait;

    public $upsheetId;

    public function mount($upsheetId)
    {
        $this->upsheetId = $upsheetId;
    }

    public function deleteShareWith()
    {
        $upsheet = UpSheet::find($this->upsheetId);
        $upsheet->share_with = null;
        $upsheet->save();

        # add activity trait
        $this->addActivity(
            "User %s Deleted Share Upsheet ",
            auth()->user()->firstname,
            auth()->user()->lastname,
            null,
            null,
            auth()->user()->id,
            $this->upsheetId,
            null
        );


        session()->flash("success", "user deleted from upsheet");
        return redirect(
            route('upsheets.show', $this->upsheetId)
        );
    }

    public function render()
    {
        return view('livewire.forms.up-sheet.delete-share-with-upsheet-form');
    }
}
