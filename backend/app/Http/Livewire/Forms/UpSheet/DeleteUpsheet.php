<?php

namespace App\Http\Livewire\Forms\UpSheet;

use App\Models\UpSheet;
use Livewire\Component;

class DeleteUpsheet extends Component
{
    public $upsheetId;

    protected $listeners=["removeUpSheet"];

    public function render()
    {
        return view('livewire.forms.up-sheet.delete-upsheet');
    }

    public function removeUpSheet($id)
    {

        $upsheet = UpSheet::find($id);
        $upsheet->delete();

        return redirect(
            route('panel.upsheets', $this->upsheetId)
        );
    }
}
