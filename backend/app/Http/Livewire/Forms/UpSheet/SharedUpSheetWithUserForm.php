<?php

namespace App\Http\Livewire\Forms\UpSheet;

use App\Models\Activity;
use App\Models\UpSheet;
use App\Models\User;
use App\Traits\ActivityTrait;
use Carbon\Carbon;
use Livewire\Component;

class SharedUpSheetWithUserForm extends Component
{
    use ActivityTrait;
    public $userId = 'none';
    public $upsheet_id;
    public $dealers;

    protected $rules = [
        'userId' => 'required|numeric'
    ];

    protected $messages = [
        "userId.numeric" => "Please select a user"
    ];

    public function mount($upsheetId)
    {
        $this->dealers = User::where('role_id', User::USER_SALER)->get();
        $this->upsheet_id = $upsheetId;
    }

    public function addSharedUserToUpsheet()
    {
        $this->validate();

        $upsheet = UpSheet::find($this->upsheet_id);
        $upsheet->share_with = intval($this->userId);
        $upsheet->save();

        # add activity trait
        $this->addActivity(
            "User %s Share Upsheet With %s ",
            auth()->user()->firstname,
            auth()->user()->lastname,
            ucwords(User::find($upsheet->share_with)->firstname),
            ucwords(User::find($upsheet->share_with)->lastname),
            auth()->user()->id,
            $this->upsheet_id,
            null
        );



        session()->flash("success", "New user added to upsheet");
        return redirect(
            route('upsheets.show', $this->upsheet_id)
        );
    }

    public function render()
    {
        return view('livewire.forms.up-sheet.shared-up-sheet-with-user-form');
    }
}
