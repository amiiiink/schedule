<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Insight extends Component
{
    public function render()
    {
        return view('livewire.insight')
            ->layout('layouts.panel');
    }
}
