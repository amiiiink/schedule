<?php

namespace App\Http\Livewire;

use App\Models\Appointment;
use App\Models\UpSheet;
use Livewire\Component;
use App\Models\Schedule as mySchedule;

class Schedule extends Component
{
    public function render()
    {
        // user auth
        $current_user = auth()->user();

        // check user is admin
        //if ($current_user->is_admin()) {
            $appointments = \App\Models\Schedule::orderBy("created_at", "ASC")->get();
            return view('livewire.schedule', compact('appointments'))
                ->layout('layouts.panel');
        //}

        // user is not admin
//        $appointments = Appointment::with("upsheet", "user", "customer")
//                            ->where('user_id', $current_user->id)
//                            ->orderBy("schedule", "ASC")
//                            ->get();
//        return view('livewire.schedule', compact('appointments'))
//            ->layout('layouts.panel');
    }
}
