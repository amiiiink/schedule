<?php

namespace App\Http\Livewire;

use App\Models\Activity;
use App\Models\Email;
use App\Traits\ActivityTrait;
use Carbon\Carbon;
use Illuminate\Mail\Message;
use Livewire\Component;
use PhpParser\Node\Expr\Throw_;
use Livewire\WithFileUploads;

class Sendemail extends Component
{
    use WithFileUploads,ActivityTrait;

    public $upsheetId;
    public $upsheet;
    public $userId;
    public $title;
    public $message = '';
    public $template;
    public $userName;
    public $email;
    public $attaches;


    public function render()
    {
        return view('livewire.sendemail');
    }



    public function changeEvent()
    {
        $this->message = $this->generateTemplate();
    }

    public function handleAddField()
    {

        $this->fields = $this->fields +1;
    }

    public function sendEmail()
    {

        if(is_null($this->email)){
            return $this->nullResponse();
        }

       try {
            $this->sendMailProcess();

            // create a instance for email sended
            Email::create([
                'user_id' => $this->userId,
                'data' => $this->message
            ]);
       }catch(\Exception $e){

            session()->flash("error", $e->getCode()."--".$e->getMessage());
            return redirect(
                route('upsheets.show', $this->upsheetId)
            );
       }

        # add activity trait
        $this->addActivity(
            "User %s Send Email To Customer %s",
            auth()->user()->lastname,
            auth()->user()->lastname,
            $this->upsheet->customer->firstname,
            $this->upsheet->customer->lastname,
            auth()->user()->id,
            $this->upsheetId,null);


        session()->flash("successMsg", "new email has been sent");
        return redirect(
            route('upsheets.show', $this->upsheetId)
        );
    }

    private function generateTemplate(): string
    {
        switch ($this->title) {
            case "(Action Plan) 1 Day - Follow Up":
                $template = "Hi $this->userName,
Thank you for giving me the opportunity to tell you about the vehicles and financing options that {DEALERSHIP.name} has to offer. Just wanted to touch base with you again since I didn’t hear back from you yesterday.
I would be happy to discuss further what you are looking for and how I can help you fulfill your needs. When is the best time to reach you?

Thank you again for your interest!";
                break;
            case "(Action Plan) 10 Day - Trying to Reac...":
                $template = "Hi $this->userName,
I have been trying to contact you regarding your next vehicle purchase. I certainly do not want to bother you, my goal is to assist you with your vehicle needs if I can. When you have a chance, please contact me and let me know where you are at in your car buying journey.

Thank you,";
                break;
            case "(Action Plan) 15 Day - In, Out, Jump":
                $template = "Hey $this->userName,
We would be honored to have you as a customer but we do not want to pester you with endless emails, texts, and voice messages. Please simply reply to this email with just one of the following words typed in the subject line to help me understand how or if I should continue to follow up with you:
*IN* - I am still \"In\" the market. I just haven't had the time to get back to you.
*OUT* - I have decided not to buy, so I'm \"Out\" of the market for now.
*BOUGHT* - Tough luck, I \"Bought\" a vehicle elsewhere, maybe next time…
*JUMP* - Leave me alone; go \"Jump\" in a lake and don't contact me again unless you are giving away cars! smiley
Since we haven't connected yet and things change quickly around here, I suggest checking out our latest inventory.
I appreciate your response and thanks again for your interest.
Take care,";
                break;
            case "(Action Plan) 3 Day - No Word Yet":
                $template = "Hi $this->userName,
How are you? I’ve been throwing a lot at you, I know. That’s only because I want to make sure you get what you need and that’s an awesome vehicle and car buying experience.
I understand the value of your time so I highly recommend setting up an appointment if you plan to visit us at {DEALERSHIP.name}. This way we can give you the attention you deserve without wasting your precious time. Let me know days and times that work for you and I will check the schedule.
I’m ready when you are! Have a good day!";
                break;

        }
        return $template;
    }

    private function nullResponse()
    {
        session()->flash("error", "this customer does not have email");
        return redirect(
            route('upsheets.show', $this->upsheetId)
        );
    }

    private function sendMailProcess(): void
    {

        if(is_null($this->attaches)) {

            $data = [];
            \Mail::send([], $data, function (Message $message) {
                $message
                    ->to($this->email, $this->userName)
                    ->from(config('mail')["mailers"]["smtp"]["username"], 'Ezlead CRM')
                    ->subject($this->title)
                    ->setBody($this->message);
            });

            return ;
        }

        $this->attaches->store('emailAttaches');
        $data = [];
        \Mail::send([], $data, function (Message $message) {
            $message
                ->to($this->email, $this->userName)
                ->from(config('mail')["mailers"]["smtp"]["username"], 'Ezlead CRM')
                ->subject($this->title)
                ->setBody($this->message)
                ->attach(\Swift_Attachment::fromPath(storage_path("app/" . $this->attaches->store('emailAttaches'))));
        });






    }
}
