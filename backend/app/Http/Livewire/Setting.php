<?php

namespace App\Http\Livewire;

use App\Models\Dealership;
use Livewire\Component;

class Setting extends Component
{
    public $name = '';
    public $timezone = '';
    public $website = '';
    public $phone_number = '';
    public $street_1 = '';
    public $street_2 = '';
    public $street_3 = '';
    public $street_4 = '';
    public $street_5 = '';
    public $close = false;
    public $country = 'america';
    public $inactive = 8;
    public $access_data = "1";
    public $user_can_see_report = false;

    public function mount()
    {
        if (!auth()->user()->is_admin()) {
            return redirect('/dashboard');
        }

        $dealership = Dealership::find(1);
        $this->name = $dealership->name;
        $this->timezone = $dealership->timezone;
        $this->website = $dealership->website;
        $this->phone_number = $dealership->phone_number;
        $this->street_1 = $dealership->street_line_one;
        $this->street_2 = $dealership->street_line_two;
        $this->street_3 = $dealership->street_line_three;
        $this->street_4 = $dealership->street_line_four;
        $this->street_5 = $dealership->street_line_five;
        $this->close = $dealership->close_in_sunday;
        $this->country = $dealership->country;
        $this->inactive = $dealership->inactive_upsheet_after_days;
        $this->access_data = $dealership->access_data;
        $this->user_can_see_report = $dealership->user_can_see_report;
    }

    public function render()
    {
        $dealership = Dealership::all();
        return view('livewire.setting', compact('dealership'))
            ->layout('layouts.panel');
    }

    public function dealershipDetailsEdit()
    {
        // dd($this->user_can_see_report);
        $dealership = Dealership::find(1);
        $dealership->name = $this->name;
        $dealership->timezone = $this->timezone;
        $dealership->website = $this->website;
        $dealership->phone_number = $this->phone_number;
        $dealership->street_line_one = $this->street_1;
        $dealership->street_line_two = $this->street_2;
        $dealership->street_line_three = $this->street_3;
        $dealership->street_line_four = $this->street_4;
        $dealership->street_line_five = $this->street_5;
        $dealership->close_in_sunday = $this->close == "on" ? true : false;
        $dealership->country = $this->country;
        $dealership->inactive_upsheet_after_days = $this->inactive;
        $dealership->access_data = $this->access_data;
        $dealership->user_can_see_report = boolval($this->user_can_see_report);
        $dealership->update();
        session()->flash('message', 'Setting successfully updated.');
    }
}
