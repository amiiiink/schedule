<?php

namespace App\Http\Livewire;

use App\Models\myJob;
use App\Models\UpSheet as ModelsUpSheet;
use App\Models\User;
use Livewire\Component;

class UpSheet extends Component
{
    public function render()
    {
//        $current_user = auth()->user();
//
//        if ($current_user->role_id != User::USER_ADMIN) {
//
//            return view('livewire.up-sheet')
//            ->layout('layouts.panel');
//        }

        $jobs=myJob::all();


        return view('livewire.up-sheet',compact('jobs'))
        ->layout('layouts.panel');
    }
}
