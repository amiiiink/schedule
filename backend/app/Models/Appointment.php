<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'schedule' => 'datetime',
    ];

    // ------------------------- Relations
    public function upsheet()
    {
        return $this->belongsTo(UpSheet::class, "up_sheet_id");
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function customer() {
        return $this->belongsTo(Customer::class);
    }

    /**
     * report of this object
     */
    public function reports()
    {
        return $this->morphMany(Report::class, 'reportable');
    }
}
