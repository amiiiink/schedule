<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dealership extends Model
{
    use HasFactory;

    const USER_ACCESS_ONLY_OWN_DATA = 1;
    const USER_ACCESS_ALL = 2;

    /**
     * mass assign
     */
    protected $guarded = [];
}
