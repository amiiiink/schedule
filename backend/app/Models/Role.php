<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Role extends Model
{
    use HasFactory;
    use Notifiable;

    /**
     * mass assign
     * @var
     */
    const ADMIN = 1;
    const Worker = 2;

    /**
     * protected request
     */
    protected $fillable = [
        'name',
        'guard_name'
    ];

    /**
     * Get user based on roles
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }
}
