<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        'reminder' => 'datetime',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function upsheet()
    {
        return $this->belongsTo(UpSheet::class);
    }

    /**
     * report of this object
     */
    public function reports()
    {
        return $this->morphMany(Report::class, 'reportable');
    }
}
