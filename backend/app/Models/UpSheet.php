<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UpSheet extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status_id',
        'temperature_id',
        'user_id',
        'customer_id'
    ];

    /**
     * Status
     *
     */
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    /**
     * temperature
     *
     */
    public function temperature()
    {
        return $this->belongsTo(Temperature::class);
    }

    /**
     * User has this upsheet
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Customer has this upsheet
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    /**
     * Appointments ralted to this model
     */
    public function appointments()
    {
        return $this->hasMany(Appointment::class)->orderBy("schedule", "ASC");
    }

    /**
     * Each upsheet comments
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * UpSheet tasks user assignd
     *
     */
    public function tasks()
    {
        return $this->hasMany(UserTask::class);
    }

    /**
     * All upsheet messages send text (sms)
     *
     */
    public function allSms()
    {
        return $this->hasMany(Message::class);
    }

    /**
     * upsheet schedules
     */
    public function schedules()
    {
        return $this->hasMany(Schedule::class);
    }

    /**
     * all files of upsheet
     */
    public function files()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    /**
     * report of this object
     */
    public function reports()
    {
        return $this->morphMany(Report::class, 'reportable');
    }

    /**
     *
     * upsheet activities
     */
    public function activities()
    {
        return $this->hasMany(Activity::class)->orderBy('created_at', 'DESC');
    }
}
