<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use HasRoles;

    protected $guard_name = 'web';

    const USER_ADMIN = 1;
    const USER_SALER = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id',
        'firstname',
        'lastname',
        'mobile',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'mobile_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    /**
     * Get the default profile photo URL if no profile photo has been uploaded.
     *
     * @return string
     */
    protected function defaultProfilePhotoUrl()
    {
        return 'https://ui-avatars.com/api/?name='.urlencode($this->firstname).'&color=7F9CF5&background=EBF4FF';
    }

    /**
     * user us admin
     */
    public function is_admin(){
        if ((int)auth()->user()->role_id === User::USER_ADMIN) {
            return true;
        }
        return false;
    }

    # attributes
    public function getFullNameAttribute()
    {
        return strtoupper($this->firstname . ' ' . $this->lastname);
    }

    /**
     * user role
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * user activities
     */
    public function activities()
    {
        return $this->hasMany(Activity::class);
    }

    public function up_sheets()
    {
        return $this->hasMany(UpSheet::class)->paginate(10);
    }

    /**
     * Get the status.
     */
    public function status()
    {
        return $this->morphMany(Status::class, 'statusable');
    }

    /**
     * get user tasks
     */
    public function tasks()
    {
        return $this->hasMany(UserTask::class);
    }

    /**
     * user schedules
     */
    public function schedules()
    {
        return $this->hasMany(Schedule::class);
    }

    /**
     * report of this object
     */
    public function reports()
    {
        return $this->morphMany(Report::class, 'reportable');
    }

    public function salesTask()
    {
        return $this->hasMany(SalesTask::class);
    }
}
