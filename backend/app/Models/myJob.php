<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class myJob extends Model
{
    use HasFactory;

    protected $fillable=["title","description"];

    public function schedules()
    {
        return $this->hasMany(Schedule::class,'up_sheet_id');
    }
}
