<?php

namespace App\Notifications;

use App\Models\Customer;
use Illuminate\Notifications\Notification;

class RegisterContactNotification extends Notification
{
    protected $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Customer $customer)
    {
        $this->data = $customer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "id" => $this->data->id,
            "title" => "New Contact",
            "type" => 'customer',
            "firstname" => $this->data->firstname,
            "lastname" => $this->data->lastname,
            "email" => $this->data->email,
            "mobile" => $this->data->mobile,
        ];
    }
}
