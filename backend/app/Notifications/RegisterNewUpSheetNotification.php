<?php

namespace App\Notifications;

use App\Models\UpSheet;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RegisterNewUpSheetNotification extends Notification
{
    protected $upsheet;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(UpSheet $upsheet)
    {
        $this->upsheet = $upsheet;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "id" => $this->upsheet->id,
            "title" => "New UpSheet",
            "type" => 'upsheet',
            "user" => $this->upsheet->user,
            "customer" => $this->upsheet->customer,
        ];
    }
}
