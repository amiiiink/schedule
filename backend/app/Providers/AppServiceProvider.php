<?php

namespace App\Providers;

use App\Facades\SMS\SMSService;
use App\Facades\File\FileService;
use App\Facades\Call\CallService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind('path.public', function() {
            return dirname(dirname(dirname(__DIR__)));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();

        // SMS Facade
        $this->app->singleton('sms', function($app) {
            return new SMSService();
        });

        // File Facade
        $this->app->singleton('fileupload', function($app) {
            return new FileService();
        });

        // File Facade
        $this->app->singleton('call', function($app) {
            return new CallService();
        });
    }
}
