<?php

namespace App\View\Components\Badge;

use App\Models\Status;
use App\Models\Temperature;
use Illuminate\View\Component;

class Tag extends Component
{
    /**
     * The Status text.
     *
     * @var string
     */
    public $text;

    /**
     * The Status color.
     *
     * @var string
     */
    public $color;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($color, $text)
    {
        $this->text = $text;
        $this->color = $color;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.badge.tag');
    }
}
