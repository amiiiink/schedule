<?php

namespace App\View\Components\Shared;

use Illuminate\View\Component;
use Illuminate\Support\Facades\Route;

class PageTitle extends Component
{
    public $route;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->route = $this->setTitle(Route::current()->uri);
    }

    private function setTitle($route) {
        $routes = explode('/', $route);

        if (in_array('upsheets', $routes)) {
            return 'upsheets';
        }

        return $routes[count($routes)-1];
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.shared.page-title');
    }
}
