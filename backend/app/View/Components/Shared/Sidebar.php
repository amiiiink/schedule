<?php

namespace App\View\Components\Shared;

use Illuminate\View\Component;

class Sidebar extends Component
{
    public $logo;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($logo)
    {
        $this->logo = $logo;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.shared.sidebar');
    }
}
