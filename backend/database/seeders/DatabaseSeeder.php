<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\Status;
use App\Models\Temperature;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // add default roles
        $roles = ['admin', 'worker'];
        foreach($roles as $role) {
            Role::create([
                "name" => $role,
                "guard_name" => 'sanctum',
            ]);
        }

        User::create([
            'role_id'=>1,
            'firstname'=>'admin',
            'lastname'=>'admin',
            'email'=>'admin@gmail.com',
            'mobile'=>"00989120814409",
            'password'=>bcrypt('123')
        ]);
        User::create([
            'role_id'=>2,
            'firstname'=>'worker',
            'lastname'=>'worker',
            'email'=>'worker@gmail.com',
            'mobile'=>"00989120814408",
            'password'=>bcrypt('123')
        ]);

    }
}
