<?php

namespace Database\Seeders;

use App\Models\Dealership;
use Illuminate\Database\Seeder;

class DealershipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Dealership::create([
            "name" => "ezautoleads"
        ]);
    }
}
