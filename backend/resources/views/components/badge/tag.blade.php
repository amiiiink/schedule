<span
    class="badge text-capitalize"
    style="background-color: {{ $color }}"
>
    {{ $text }}
</span>
