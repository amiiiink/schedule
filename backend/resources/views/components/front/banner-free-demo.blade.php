<section id="demo">
    <div class="container mt-5 pt-5 pb-2">
        <div class="row mt-5 pt-5 pb-5">
            <div class="col-md-12">
                <div class="sec-demo d-flex align-items-center">
                    <div class="d-inline-block">
                        <h2>Or Explore AutoRaptor's Dealer CRM Features</h2>
                        <p>Sign up for a ezlead account or ask to see our platform in action and discover the
                            functionalities.</p>
                    </div>
                    <div class="d-inline-block">
                        <a href="#">Request a Free Demo</a>
                    </div>
                    <img src="/front/images/demo.png" alt="ezlead" data-aos="zoom-out-down">
                </div>
            </div>
        </div>
    </div>
</section>
