<!-- brands and news-->
<div class="banner"></div>
<section id="news">
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="text-center brands-svg" data-aos="fade-up">
                    <img src="/front/images/icons/brands.svg" alt="ezlead">
                </div>
                <div class="text-left" data-aos="fade-up">
                    <h2>Experience <span>5,000</span> Brands Are Already Using Ezlead to improve their customer
                        experience</h2>
                </div>
                <div class="mt-5 text-center a-brands pb-5" data-aos="fade-up">
                    <a href="#">Show all customers <img src="/front/images/icons/right-arrow.png" alt="arrow"></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="blog" class="text-center brands-svg mt-5 pt-5" data-aos="fade-up">
                    <img src="/front/images/icons/news.svg" alt="ezlead" data-aos="fade-up">
                    <h2 data-aos="fade-up">The latest news and updates for our services</h2>
                </div>
            </div>
        </div>

        <div class="row mt-5 mb-5 pb-5">
            <div class="owl-carousel owl-theme news-carousel d-flex align-items-stretch" data-aos="zoom-out-up">
                <div class="item item-news-carousel">
                    <div class="img-news-carousel" style="background-image: url('/front/images/slide1.jpg')"></div>
                    <h4>What is Ezlead?</h4>
                    <p>Learn more about the integrated CRM platform that gives all your departments a single, shared
                        view of every customer.</p>
                    <div class="news-link text-center">
                        <a href="#">Learn more <img src="/front/images/icons/right-arrow.png" alt="arrow"></a>
                    </div>
                </div>

                <div class="item item-news-carousel">
                    <div class="img-news-carousel" style="background-image: url('/front/images/slide2.jpg')"></div>
                    <h4>What is Ezlead?</h4>
                    <p>Learn more about the integrated CRM platform that gives all your departments a single, shared
                        view of every customer.</p>
                    <div class="news-link text-center">
                        <a href="#">Learn more <img src="/front/images/icons/right-arrow.png" alt="arrow"></a>
                    </div>
                </div>

                <div class="item item-news-carousel">
                    <div class="img-news-carousel" style="background-image: url('/front/images/slide3.jpg')"></div>
                    <h4>What is Ezlead?</h4>
                    <p>Learn more about the integrated CRM platform that gives all your departments a single, shared
                        view of every customer.</p>
                    <div class="news-link text-center">
                        <a href="#">Learn more <img src="/front/images/icons/right-arrow.png" alt="arrow"></a>
                    </div>
                </div>

                <div class="item item-news-carousel">
                    <div class="img-news-carousel" style="background-image: url('/front/images/cust2.jpg')"></div>
                    <h4>What is Ezlead?</h4>
                    <p>Learn more about the integrated CRM platform that gives all your departments a single, shared
                        view of every customer.</p>
                    <div class="news-link text-center">
                        <a href="#">Learn more <img src="/front/images/icons/right-arrow.png" alt="arrow"></a>
                    </div>
                </div>

                <div class="item item-news-carousel">
                    <div class="img-news-carousel" style="background-image: url('/front/images/slide1.jpg')"></div>
                    <h4>What is Ezlead?</h4>
                    <p>Learn more about the integrated CRM platform that gives all your departments a single, shared
                        view of every customer.</p>
                    <div class="news-link text-center">
                        <a href="#">Learn more <img src="/front/images/icons/right-arrow.png" alt="arrow"></a>
                    </div>
                </div>

                <div class="item item-news-carousel">
                    <div class="img-news-carousel" style="background-image: url('/front/images/cust1.jpeg')"></div>
                    <h4>What is Ezlead?</h4>
                    <p>Learn more about the integrated CRM platform that gives all your departments a single, shared
                        view of every customer.</p>
                    <div class="news-link text-center">
                        <a href="#">Learn more <img src="/front/images/icons/right-arrow.png" alt="arrow"></a>
                    </div>
                </div>

                <div class="item item-news-carousel">
                    <div class="img-news-carousel" style="background-image: url('/front/images/cust3.jpg')"></div>
                    <h4>What is Ezlead?</h4>
                    <p>Learn more about the integrated CRM platform that gives all your departments a single, shared
                        view of every customer.</p>
                    <div class="news-link text-center">
                        <a href="#">Learn more <img src="/front/images/icons/right-arrow.png" alt="arrow"></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
