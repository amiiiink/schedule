<!-- customers-->
<section id="customers" class="pb-4 position-relative">
    <div class="customers-icons">
        <i class="fa fa-circle"></i>
        <i class="fa fa-circle"></i>
        <i class="fa fa-circle"></i>
        <i class="fa fa-circle"></i>
        <i class="fa fa-circle"></i>
        <i class="fa fa-circle"></i>
    </div>
    <div class="container">
        <div class="text-center" data-aos="zoom-in">
            <h2>What Our Customers Are Saying</h2>
        </div>

        <div class="row mt-5 pt-5">
            <div class="col-md-4 d-flex align-items-stretch">
                <div class="card-customers" data-aos="flip-right">
                    <div class="img-customer" style="background-image: url('/front/images/cust1.jpeg')"></div>
                    <h4>Zoe Miller</h4>
                    <div class="text-center">
                        <span>Carmix Auto Sales</span>
                    </div>
                    <p class="customers-title">I really like how easy it is to develop 2 way communication with
                        customers using AutoRaptor.</p>
                    <p class="customers-caption">Everything is in one place and easy to navigate. Keeping track of
                        lost customers is easier than ever as well. AutoRaptor is well worth the money and can
                        really ramp up your customer interactions/sales.</p>
                </div>
            </div>
            <div class="col-md-4 d-flex align-items-stretch">
                <div class="card-customers" data-aos="flip-right" data-aos-delay="300">
                    <div class="img-customer" style="background-image: url('/front/images/cust2.jpg')"></div>
                    <h4>Susan Anton</h4>
                    <div class="text-center">
                        <span>Ruby Product Manager</span>
                    </div>
                    <p class="customers-title">What I like most about AutoRaptor is the ability to interact with
                        customers so easily, especially the pre-built templates.</p>
                    <p class="customers-caption">The graphs make it easy to manage work progression and to really
                        see if we are doing great or not. The stat sheet makes it convenient to track lost sales and
                        sold inventory.</p>
                </div>
            </div>
            <div class="col-md-4 d-flex align-items-stretch">
                <div class="card-customers" data-aos="flip-right" data-aos-delay="600">
                    <div class="img-customer" style="background-image: url('/front/images/cust3.jpg')"></div>
                    <h4>Susie Amis</h4>
                    <div class="text-center">
                        <span>Senior Strategy Advisor</span>
                    </div>
                    <p class="customers-title">AutoRaptor is the single most valuable tool we have in our toolbox
                        here at Lake City Exports!</p>
                    <p class="customers-caption">AutoRaptor is the single most valuable tool we have in our toolbox
                        here at Lake City Exports! Raptor is super easy to use, packed with features, and its
                        integration with all of our other platforms is seamless.</p>
                </div>
            </div>
        </div>
    </div>
</section>
