<section id="crm">
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="text-center" data-aos="zoom-in">
                    <h2>We’d love to show you how the Ezlead CRM can set you up for success</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="img-crm mt-5 text-center" data-aos="fade-up">
                    <img src="/front/images/crm.jpg" class="img-fluid" alt="ezlead">
                    <div class="mt-5 text-center">
                        <a href="#">Learn more <img src="/front/images/icons/right-arrow.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Capterra-->
<section id="capterra">
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="text-center" data-aos="zoom-in">
                    <h2>Capterra's Big Book of Free Software Has Hundreds
                        of Tools You Can Try Today, For Free</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="img-crm mt-5 text-center" data-aos="fade-up">
                    <img src="/front/images/Capterra.jpg" class="img-fluid" alt="ezlead">
                    <div class="mt-5 text-center">
                        <a href="#">Learn more <img src="/front/images/icons/right-arrow.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
