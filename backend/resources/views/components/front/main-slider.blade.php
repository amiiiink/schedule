<!--header main page-->
<div class="header-main-page d-flex align-items-center">
    <div class="container">
        <div class="row" data-aos="fade-up">
            <div class="col-md-5 align-self-center">
                <div class="haeder-main-text">
                    <h2>CRM for Independent andBHPH Dealerships.</h2>
                    <p>Allowing auto dealers to implement an effective sales process by capturing leads from all
                        sources in one place. Powerful features like texting, VIN & license scanning, email
                        marketing, and reporting are just the beginning.</p>
                    <a class="header-main-link1" href="{{ route('login') }}">CRM Starter Pack</a>
                    <a class="header-main-link2" href="#">Watch Demos</a>
                </div>
            </div>
            <div class="col-md-7">
                <div class="owl-carousel owl-theme top-carousel">
                    <a href="#">
                        <div class="item item-top-carousel"
                            style="background-image: url('/front/images/g1.jpg')">
                        </div>
                    </a>
                    <a href="#">
                        <div class="item item-top-carousel"
                            style="background-image: url('/front/images/g2.jpg')">
                        </div>
                    </a>
                    <a href="#">
                        <div class="item item-top-carousel"
                            style="background-image: url('/front/images/g4.jpg')">
                        </div>
                    </a>
                    <a href="#">
                        <div class="item item-top-carousel"
                            style="background-image: url('/front/images/g5.jpg')">
                        </div>
                    </a>
                    <a href="#">
                        <div class="item item-top-carousel"
                            style="background-image: url('/front/images/g6.jpg')">
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!--        scroll arrow-->
    <a class="header-scroll-arrow" href="#pricing"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
</div>
