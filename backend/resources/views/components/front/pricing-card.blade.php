<!-- pricing-->
<section id="pricing">
    <div class="container">
        <div class="text-center" data-aos="zoom-in">
            <h2>Explore all the solutions in Ezlead Customer</h2>
        </div>

        <ul class="nav nav-pills nav-pills-price mb-5 pb-3" id="pills-tab" role="tablist" data-aos="zoom-in">
            <li class="nav-item">
                <a class="nav-link active" id="price-need-tab" data-toggle="pill" href="#price-need" role="tab"
                    aria-controls="price-need" aria-selected="true">By Need</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="price-sales-tab" data-toggle="pill" href="#price-sales" role="tab"
                    aria-controls="price-sales" aria-selected="false">Sales</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="price-services-tab" data-toggle="pill" href="#price-services" role="tab"
                    aria-controls="price-services" aria-selected="false">Service</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="price-marketing-tab" data-toggle="pill" href="#price-marketing" role="tab"
                    aria-controls="price-marketing" aria-selected="false">Marketing</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="price-commerce-tab" data-toggle="pill" href="#price-commerce" role="tab"
                    aria-controls="price-commerce" aria-selected="false">Commerce</a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="price-need" role="tabpanel" aria-labelledby="price-need-tab">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up">
                            <img src="/front/images/icons/price1.jpg" alt="ezlead">
                            <h3>Text Messaging</h3>
                            <p>Leverage the power of mobile texting; respond to leads faster and keep them engaged.
                                Communication is tracked and logged for reporting and accountability.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up" data-aos-delay="300">
                            <img src="/front/images/icons/price2.jpg" alt="ezlead">
                            <h3>Full Suite Web-App</h3>
                            <p>Our dealer CRM app can scan customer licenses and VIN barcodes. Data is available in
                                the Web app immediately, ready for tracking and to close sales.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up" data-aos-delay="600">
                            <img src="/front/images/icons/price3.jpg" alt="ezlead">
                            <h3>Email Campaigns</h3>
                            <p>Boost your marketing and reach your customers with custom blasts on existing or
                                custom templates. Filter customers by interest and status in the sales process.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up" data-aos-delay="900">
                            <img src="/front/images/icons/price4.jpg" alt="ezlead">
                            <h3>Scheduling & Automation</h3>
                            <p>Keep your sales team on track and motivated with action plans, custom workflows,
                                tasks, and push notification reminders to reach them wherever they are.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="price-sales" role="tabpanel" aria-labelledby="price-sales-tab">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up">
                            <img src="/front/images/icons/price1.jpg" alt="ezlead">
                            <h3>Text Messaging</h3>
                            <p>Leverage the power of mobile texting; respond to leads faster and keep them engaged.
                                Communication is tracked and logged for reporting and accountability.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up" data-aos-delay="300">
                            <img src="/front/images/icons/price2.jpg" alt="ezlead">
                            <h3>Full Suite Web-App</h3>
                            <p>Our dealer CRM app can scan customer licenses and VIN barcodes. Data is available in
                                the Web app immediately, ready for tracking and to close sales.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up" data-aos-delay="600">
                            <img src="/front/images/icons/price3.jpg" alt="ezlead">
                            <h3>Email Campaigns</h3>
                            <p>Boost your marketing and reach your customers with custom blasts on existing or
                                custom templates. Filter customers by interest and status in the sales process.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up" data-aos-delay="900">
                            <img src="/front/images/icons/price4.jpg" alt="ezlead">
                            <h3>Scheduling & Automation</h3>
                            <p>Keep your sales team on track and motivated with action plans, custom workflows,
                                tasks, and push notification reminders to reach them wherever they are.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="price-services" role="tabpanel" aria-labelledby="price-services-tab">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up">
                            <img src="/front/images/icons/price1.jpg" alt="ezlead">
                            <h3>Text Messaging</h3>
                            <p>Leverage the power of mobile texting; respond to leads faster and keep them engaged.
                                Communication is tracked and logged for reporting and accountability.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up" data-aos-delay="300">
                            <img src="/front/images/icons/price2.jpg" alt="ezlead">
                            <h3>Full Suite Web-App</h3>
                            <p>Our dealer CRM app can scan customer licenses and VIN barcodes. Data is available in
                                the Web app immediately, ready for tracking and to close sales.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up" data-aos-delay="600">
                            <img src="/front/images/icons/price3.jpg" alt="ezlead">
                            <h3>Email Campaigns</h3>
                            <p>Boost your marketing and reach your customers with custom blasts on existing or
                                custom templates. Filter customers by interest and status in the sales process.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up" data-aos-delay="900">
                            <img src="/front/images/icons/price4.jpg" alt="ezlead">
                            <h3>Scheduling & Automation</h3>
                            <p>Keep your sales team on track and motivated with action plans, custom workflows,
                                tasks, and push notification reminders to reach them wherever they are.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="price-marketing" role="tabpanel" aria-labelledby="price-marketing-tab">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up">
                            <img src="/front/images/icons/price1.jpg" alt="ezlead">
                            <h3>Text Messaging</h3>
                            <p>Leverage the power of mobile texting; respond to leads faster and keep them engaged.
                                Communication is tracked and logged for reporting and accountability.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up" data-aos-delay="300">
                            <img src="/front/images/icons/price2.jpg" alt="ezlead">
                            <h3>Full Suite Web-App</h3>
                            <p>Our dealer CRM app can scan customer licenses and VIN barcodes. Data is available in
                                the Web app immediately, ready for tracking and to close sales.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up" data-aos-delay="600">
                            <img src="/front/images/icons/price3.jpg" alt="ezlead">
                            <h3>Email Campaigns</h3>
                            <p>Boost your marketing and reach your customers with custom blasts on existing or
                                custom templates. Filter customers by interest and status in the sales process.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up" data-aos-delay="900">
                            <img src="/front/images/icons/price4.jpg" alt="ezlead">
                            <h3>Scheduling & Automation</h3>
                            <p>Keep your sales team on track and motivated with action plans, custom workflows,
                                tasks, and push notification reminders to reach them wherever they are.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="price-commerce" role="tabpanel" aria-labelledby="price-commerce-tab">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up">
                            <img src="/front/images/icons/price1.jpg" alt="ezlead">
                            <h3>Text Messaging</h3>
                            <p>Leverage the power of mobile texting; respond to leads faster and keep them engaged.
                                Communication is tracked and logged for reporting and accountability.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up" data-aos-delay="300">
                            <img src="/front/images/icons/price2.jpg" alt="ezlead">
                            <h3>Full Suite Web-App</h3>
                            <p>Our dealer CRM app can scan customer licenses and VIN barcodes. Data is available in
                                the Web app immediately, ready for tracking and to close sales.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up" data-aos-delay="600">
                            <img src="/front/images/icons/price3.jpg" alt="ezlead">
                            <h3>Email Campaigns</h3>
                            <p>Boost your marketing and reach your customers with custom blasts on existing or
                                custom templates. Filter customers by interest and status in the sales process.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-price" data-aos="fade-up" data-aos-delay="900">
                            <img src="/front/images/icons/price4.jpg" alt="ezlead">
                            <h3>Scheduling & Automation</h3>
                            <p>Keep your sales team on track and motivated with action plans, custom workflows,
                                tasks, and push notification reminders to reach them wherever they are.</p>
                            <a class="price-learn-more" href="#">Learn more</a>
                            <div class="price-label">
                                <a href="">Buy now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
