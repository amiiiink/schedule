<footer>
    <div class="footer clearfix mb-0 text-muted">
        <div class="float-left">
            <p>2021 &copy; Schedule</p>
        </div>
        <div class="float-right">
            <p>By
                <a href="#">Amin Karimi</a>
            </p>
        </div>
    </div>
</footer>
