<nav class="navbar navbar-header navbar-expand navbar-light">
    <a class="sidebar-toggler" href="javascript:void(0)"><span class="navbar-toggler-icon"></span></a>
    <button class="btn navbar-toggler" type="button" data-toggle="collapse"
        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
        aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav d-flex align-items-center navbar-light ml-auto">
            <li class="dropdown nav-icon">
                <a href="#" data-toggle="dropdown"
                    class="nav-link  dropdown-toggle nav-link-lg nav-link-user">
                    <div class="d-lg-inline-block relative">
                        <i data-feather="bell" style="width: 17px;height:17px;"></i>
                        <small class="badge text-red absolute" style="left:-10px;bottom:-10px;" ></small>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-large">
                    <h6 class='py-2 px-4'>Notifications</h6>
                    <ul class="list-group rounded-none" style="max-height: 500px !important;overflow-y:scroll">
                            <li class="list-group-item border-0 mt-4 border-t-2 text-right">
                                Not Notification Yet
                            </li>
                    </ul>
                </div>
            </li>
            <li class="nav-icon mr-2">
                <a href="#"
                    class="nav-link nav-link-lg nav-link-user">
                    <div class="d-lg-inline-block">
                        <i data-feather="mail"></i>
                    </div>
                </a>
            </li>
            <li class="dropdown">
                <a href="#" data-toggle="dropdown"
                    class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                    @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                        <div class="avatar mr-1">
                            <img class="h-10 w-10 rounded-full object-cover" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->firstname }}" />
                        </div>
                    @endif
                    <div class="d-none d-md-block d-lg-inline-block">Hi, {{ Auth::user()->firstname }}</div>
                </a>
                <div class="dropdown-menu dropdown-menu-right" style="max-height: 600px !important;overflow-y: scroll !important;">
                    <a class="dropdown-item" href="{{ url('/user/profile') }}"><i data-feather="user"></i> Profile</a>

                    <div class="dropdown-divider"></div>
                    <form method="POST" action="{{ route('logout') }}" id="logout">
                        @csrf
                        <button class="dropdown-item active" href="javascript:void(0)"
                        onclick="document.getElementById('logout').submit()"><i data-feather="log-out"></i>&nbsp;Logout</button>
                    </form>
                </div>
            </li>
        </ul>
    </div>
</nav>
