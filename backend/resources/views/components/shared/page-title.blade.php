<div class="page-title mb-5">
    <div class="row justify-content-between align-items-center">
        <div class="col-5">
            <p class="text-capitalize my-0 w-100 font-extrabold"></p>
        </div>
        <div class="col-7 text-right">
            @if(Auth::user()->role->name == "admin")
            <a class="btn btn-danger text-capitalize px-2 py-1 font-bold" href="{{ route('panel.upsheets.create') }}">add a new job</a>
            @endif
            {{-- <a class="btn btn-secondary btn-lg mx-2" href="vehicle/create">New Vehicle</a> --}}
        </div>
    </div>
</div>
