<div id="sidebar" class="active">
    <div class="sidebar-wrapper active bg-yellow-100" >
        <div class="sidebar-header">
            {{ Auth::user()->role->name }}
        </div>
        <div class="sidebar-menu">
            <ul class="menu">
                <li class='sidebar-title'>Main Menu</li>
                <li class="sidebar-item {{ request()->routeIs('dashboard') ? 'active' : '' }}">
                    <a href="{{ url('/dashboard') }}" class='sidebar-link'>
                        <i data-feather="home" width="20"></i>
                        <span>Dashboard</span>
                    </a>
                </li>



                <li class="sidebar-item {{ request()->routeIs('panel.upsheets') ? 'active' : '' }}">
                    <a href="{{ route('panel.upsheets') }}" class="sidebar-link">
                        <i data-feather="file-text" width="20"></i>
                        <span>Jobs</span>
                    </a>
                </li>



                @if(Auth::user()->role->name == "admin")
                    <li class="sidebar-item {{ request()->routeIs('panel.schedules') ? 'active' : '' }}">
                        <a href="{{ route('panel.schedules') }}" class="sidebar-link">
                            <i data-feather="calendar" width="20"></i>
                            <span>Schedule</span>
                        </a>
                    </li>
                    <li class='sidebar-title'>Users</li>
                    <li class="sidebar-item {{ request()->routeIs('permission.index') ? 'active' : '' }}">
                        <a href="{{ route('permission.index') }}" class='sidebar-link'>
                            <i data-feather="settings" width="20"></i>
                            <span>Users/Roles</span>
                        </a>
                    </li>



                @endif
            </ul>
        </div>
        <button class="sidebar-toggler btn x">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                stroke-linejoin="round" class="feather feather-x" style="color: white">
                <line x1="18" y1="6" x2="6" y2="18"></line>
                <line x1="6" y1="6" x2="18" y2="18"></line>
            </svg>
        </button>
    </div>
</div>
