<x-panel-layout>
    <section class="section">
        @if(Auth::user()->role_id == \App\Models\User::USER_ADMIN)

            <div class="row mb-4">
                <div class="col-12">
                    <div class="card bg-white shadow">
                        <div class="card-body">
                            <h3 class="text-white">Total Account</h3>
                            <div class="card-group">
                                <div class="card shadow-sm bg-gray-400">
                                    <div class="card-content">
                                        <div class="card-body bg-yellow-100">
                                            <h4 class="card-title">JOBS</h4>
                                            <h3>{{ \App\Models\myJob::count() }}</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="card shadow-sm">
                                    <div class="card-content">
                                        <div class="card-body bg-gray-200">
                                            <h4 class="card-title">SCHEDULED</h4>
                                            <h3>{{ \App\Models\Schedule::count() }}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        @endif
    </section>
</x-panel-layout>
