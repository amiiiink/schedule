<x-panel-layout>
    <section class="section">
        <div class="row mb-2">
            <a class="btn btn-primary w-25 mb-3 btn-sm" href="{{ route('dashboard') }}">Back To Dashboard</a>
            <div class="col-12 table-responsive bg-white rounded shadow-sm">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>SHOW</th>
                            <th>created_at</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($ads as $ad)
                            <tr>
                                <td>{{ $loop->index+1 }}</td>
                                <td>
                                    <a class="btn btn-primary" href="{{ route('ad-single', $ad->id) }}">SHOW</a>
                                </td>
                                <td>{{ $ad->created_at->format('Y, m d H:i') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="my-4 d-flex justify-content-center">
                {{ $ads->links() }}
            </div>

        </div>
    </section>
</x-panel-layout>
