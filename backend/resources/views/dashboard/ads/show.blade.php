<x-panel-layout>
    <section class="section">
        <div class="row mb-2">
            <a class="btn btn-primary w-25 mb-3 btn-sm" href="{{ route('ad-show') }}">Back To Dashboard</a>
            <div class="col-12 table-responsive bg-white rounded shadow-sm">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Question</th>
                            <th class="w-25">Answer</th>
                            <th>extra</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                Is your dealership already active on Social Media? If so on which Platforms
                            </td>
                            <td>
                                @foreach($data->social as $d)
                                    <p>{{ $d }}</p>
                                @endforeach
                            </td>
                            <td>{{ $data->social_extra }}</td>
                        </tr>
                        <tr>
                            <td>
                                Exposure hoping to achieve from Marketing?
                            </td>
                            <td>
                                @foreach($data->achieve as $d)
                                    <p>{{ $d }}</p>
                                @endforeach
                            <td>{{ $data->achieve_extra }}</td>
                        </tr>
                        <tr>
                            <td>
                                Vehicle type Sold at Dealership
                            </td>
                            <td>
                                @foreach($data->vehicle_type as $d)
                                    <p>{{ $d }}</p>
                                @endforeach
                            <td>{{ $data->vehicle_type_extra }}</td>
                        </tr>
                        <tr>
                            <td>
                                Types of financing at dealership
                            </td>
                            <td>
                                @foreach($data->finance_type as $d)
                                    <p>{{ $d }}</p>
                                @endforeach
                            <td>{{ $data->finance_type_extra }}</td>
                        </tr>
                        <tr>
                            <td>
                                Target Customers
                            </td>
                            <td>
                                @foreach($data->target_customer as $d)
                                    <p>{{ $d }}</p>
                                @endforeach
                            <td>{{ $data->target_customer_extra }}</td>
                        </tr>
                        <tr>
                            <td>
                                Geographical Targeting for locations of ads to be placed (If owner does not know we can offer a service where we go through history of sales develop a heat map of where most of sales are coming from to be able to improve targeting)
                            </td>
                            <td>
                                @foreach($data->location as $d)
                                    <p>{{ $d }}</p>
                                @endforeach
                            <td>{{ $data->location_extra }}</td>
                        </tr>
                        <tr>
                            <td>
                                New VA use bardare
                            </td>
                            <td>
                                @foreach($data->vehicle_sell as $d)
                                    <p>{{ $d }}</p>
                                @endforeach
                            <td>{{ $data->vehicle_sell_extra }}</td>
                        </tr>
                        <tr>
                            <td>
                                Ads Language
                            </td>
                            <td>
                                @foreach($data->language as $d)
                                    <p>{{ $d }}</p>
                                @endforeach
                            <td>{{ $data->language_extra }}</td>
                        </tr>
                        <tr>
                            <td>
                                Specific type of vehicle hoping to sell from Paid Ads
                            </td>
                            <td>
                                @foreach($data->daily_budget as $d)
                                    <p>{{ $d }}</p>
                                @endforeach
                            <td>{{ $data->daily_budget_extra }}</td>
                        </tr>
                        <tr>
                            <td>
                                Additional Comments or Requests
                            </td>
                            <td>{{ $data->extra_comment }}</td>
                            <td> - </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </section>
</x-panel-layout>
