<x-panel-layout>
    <section class="section">
        <div class="row mb-2">
            <div class="col-12 table-responsive bg-white rounded shadow-sm">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>from</th>
                            <th>message</th>
                            <th>to</th>
                            <th>send_at</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($messages as $message)
                            <tr>
                                <td>{{ $message->user->firstname . " " . $message->user->lastname }}</td>
                                <td>{{ $message->message }}</td>
                                <td>
                                    <span class="bg-yellow-300 p-2 rounded-md text-white font-bold">{{ $message->customer->mobile }}</span>
                                    :&nbsp;<span>{{ $message->customer->firstname }} {{ $message->customer->lastname }}</span>
                                </td>
                                <td>{{ $message->created_at }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <x-slot name="links">
        <link rel="stylesheet" href="/panel/assets/vendors/choices.js/choices.min.css" />
    </x-slot>

    <x-slot name="scripts">
        <script src="/panel/assets/vendors/choices.js/choices.min.js" defer></script>
    </x-slot>
</x-panel-layout>
