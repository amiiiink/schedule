<x-panel-layout>
    <section class="section">
        <div class="row mb-2">
            <div class="col-12 table-responsive bg-white rounded shadow-sm">
                <table class="table table-striped text-center">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>User</th>
                            <th>created_at</th>
                            <th>Link</th>
                            <th>Mark As Read</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(Auth::user()->unreadNotifications->count() > 0)
                            @foreach(Auth::user()->unreadNotifications as $notification)
                                <tr>
                                    @switch($notification->data['type'])
                                        @case('customer'):
                                            <td>
                                                {{ $notification->data['title'] }}
                                            </td>
                                            <td>
                                                {{ $notification->data['firstname'] . ' ' . $notification->data['lastname'] }}
                                            </td>
                                            <td>
                                                {{ $notification->created_at }}
                                            </td>
                                            <td>
                                                <a class="text-sm text-blue-600 hover:text-white bg-gray-200 hover:bg-gray-900 p-2 rounded" href="/customer/{{ $notification->data['id'] }}">customer link</a>
                                            </td>
                                            <td>
                                                <form action="{{ route("mark-single-notification", $notification->id) }}" method="POST">
                                                    @csrf
                                                    <button class="text-sm text-yellow-600 hover:text-white bg-gray-200 hover:bg-gray-900 p-2 rounded w-50" type="submit">Read</button>
                                                </form>
                                            </td>
                                        @case('upsheet'):
                                            <td>
                                                {{ $notification->data['title'] }}
                                            </td>
                                            <td class="text-capitalize">
                                                {{ $notification->data['customer']['firstname'] . ' ' . $notification->data['customer']['lastname'] }}
                                            </td>
                                            <td>
                                                {{ $notification->created_at->format('Y F d H:i') }}
                                            </td>
                                            <td class="text-capitalize">
                                                <a class="text-sm text-red-600 hover:text-white bg-gray-200 hover:bg-gray-900 p-2 rounded" href="/dashboard/upsheets/{{ $notification->data['id'] }}">upsheet see more</a>
                                            </td>
                                            <td>
                                                <form action="{{ route("mark-single-notification", $notification->id) }}" method="POST">
                                                    @csrf
                                                    <button class="text-sm text-yellow-600 hover:text-white bg-gray-200 hover:bg-gray-900 p-2 rounded w-50" type="submit">Read</button>
                                                </form>
                                            </td>
                                            @break
                                    @endswitch
                                </tr>
                            @endforeach
                            <li class="list-group-item border-0 mt-4 border-t-2 text-right">
                                <a class="text-sm text-red-500 hover:text-red-900 w-100" href="{{ route('read-notification') }}">mark all as read</a>
                            </li>
                        @else
                            <li class="list-group-item border-0 mt-4 border-t-2 text-right">
                                Not Notification Yet
                            </li>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <x-slot name="links">

    </x-slot>

    <x-slot name="scripts">

    </x-slot>
</x-panel-layout>
