<x-panel-layout>
    <section class="section">
        <div class="row mb-2">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-capitalize">assign permission</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('permission.assign.role.store') }}" method="POST">
                        @csrf
                        <div class="form-group mb-5">
                            <select class="choices form-select" name="role" id="roles">
                                <option value="" selected>Choose Role</option>
                                @foreach($roles as $role)
                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="choices multiple-remove form-select" name="permissions[]" id="permissions" multiple>
                                <option value="">Choose Permission</option>
                                @foreach($permissions as $permission)
                                    <option value="{{ $permission->id }}" selected>{{ $permission->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-success text-capitalize">assign</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <x-slot name="links">
        <link rel="stylesheet" href="/panel/assets/vendors/choices.js/choices.min.css" />
    </x-slot>

    <x-slot name="scripts">
        <script src="/panel/assets/vendors/choices.js/choices.min.js" defer></script>
    </x-slot>
</x-panel-layout>
