<x-panel-layout>
    <section class="section">
        <div class="row mb-2">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-capitalize">create permission</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('permission.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="name" class="mb-2">Name</label>
                            <input type="text" name="name" id="name" class="form-control" focus />
                        </div>
                        <div>
                            <button type="submit" class="btn btn-success text-capitalize">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</x-panel-layout>
