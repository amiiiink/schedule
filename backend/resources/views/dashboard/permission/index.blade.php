<x-panel-layout>

<section class="section">
    <div class="row mb-2">
        <div class="card">
{{--            <div class="card-header">--}}
{{--                <a class="btn btn-dark btn-lg text-capitalize" href="{{ route('permission.create') }}">create permission</a>--}}
{{--                <a class="btn btn-info btn-lg text-capitalize" href="{{ route('permission.assign.role') }}">assign permission to role</a>--}}
{{--            </div>--}}
            <div class="card-body  table-responsive">
{{--                <div class="mb-5 mb-1">--}}
{{--                    <p>All Permissions</p>--}}
{{--                    @foreach($permissions as $permission)--}}
{{--                        <span class="badge bg-danger mr-2 rounded-pill">{{ $permission->name }}</span>--}}
{{--                    @endforeach--}}
{{--                </div>--}}

{{--                <p>Permissions Assigned To User</p>--}}
                <p>Users and Roles</p>
                <table class="table table-bordered table-striped table-hover mb-5">
                    <thead>
                        <tr>
                            <th>User</th>
                            <th>Role</th>
{{--                            <th>Permissions</th>--}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->firstname }}</td>
                                <td>{{ $user->role->name }}</td>
{{--                                <td>--}}
{{--                                    @foreach($user->getAllPermissions() as $permission)--}}
{{--                                        <span class="badge bg-info mr-2 rounded-0">{{ $permission->name }}</span>--}}
{{--                                    @endforeach--}}
{{--                                </td>--}}
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $users->links() }}

{{--                <p>Permissions Assigned To Role</p>--}}
{{--                <table class="table table-bordered table-striped table-hover">--}}
{{--                    <thead>--}}
{{--                        <tr>--}}
{{--                            <th>Role</th>--}}
{{--                            <th>Permissions</th>--}}
{{--                        </tr>--}}
{{--                    </thead>--}}
{{--                    <tbody>--}}
{{--                        @foreach($roles as $role)--}}
{{--                            <tr>--}}
{{--                                <td>{{ $role->name }}</td>--}}
{{--                                <td>--}}
{{--                                    @foreach($role->permissions as $permission)--}}
{{--                                        <span class="badge bg-info mr-2 rounded-0">{{ $permission->name }}</span>--}}
{{--                                    @endforeach--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                        @endforeach--}}
{{--                    </tbody>--}}
{{--                </table>--}}
            </div>
            {{ $roles->links() }}
        </div>
    </div>
</section>

</x-panel-layout>
