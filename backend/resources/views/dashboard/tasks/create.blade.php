<x-panel-layout>
    <section class="section">
        <div class="row mb-2">
            <div class="card">
                <div class="card-body table-responsive">
                    <div class="row justify-center align-middle">
                        <div class="col-sm-12 col-md-6">
                            <form action="{{ route('tasklist.store') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="users">Users</label>
                                    <select name="user_id" id="users" class="form-control">
                                        @foreach($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->firstname. ' ' . $user->lastname }}</option>
                                        @endforeach
                                    </select>
                                    @error('user_id')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="text">Task for user</label>
                                    <textarea name="text" id="text" cols="30" rows="10" class="form-control"></textarea>
                                    @error('text')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="bg-blue-400 hover:bg-blue-500 hover:shadow-lg text-white py-2 px-10 rounded-md text-capitalize">save</button>
                                    <a class="bg-purple-400 hover:bg-purple-500 hover:shadow-lg p-2 rounded-md text-white text-capitalize font-bold" href="{{ route('tasklist.index') }}">back to tasks</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-panel-layout>
