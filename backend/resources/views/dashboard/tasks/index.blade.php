<x-panel-layout>

    <section class="section">
        <div class="row mb-2">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-dark text-capitalize font-bold" href="{{ route('tasklist.create') }}">create tasks</a>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered table-striped table-hover mb-5">
                        <thead>
                            <tr>
                                <th class="text-uppercase">User</th>
                                <th class="text-uppercase">task_count</th>
                                <th class="text-uppercase">detail</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td class="text-capitalize">{{ $user->firstname . ' ' . $user->lastname }}</td>
                                    <td>{{ $user->salesTask->count() }}</td>
                                    <td>
                                        <a class="underline text-capitalize" href="{{ route('tasklist.show', $user->id) }}">show</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </section>

</x-panel-layout>
