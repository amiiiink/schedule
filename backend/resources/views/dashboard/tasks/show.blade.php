<x-panel-layout>
    <section class="section">
        <div class="row mb-2">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-dark text-capitalize font-bold" href="{{ route('tasklist.index') }}">back to List</a>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered table-striped table-hover mb-5">
                        <thead>
                            <tr>
                                <th class="text-uppercase">Task</th>
                                <th class="text-uppercase">Done</th>
                                <th class="text-uppercase">Assigned at</th>
                                <th class="text-uppercase">check</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($tasks) > 0)
                                @foreach ($tasks as $task)
                                    <tr>
                                        <td class="{{ $task->is_done ? 'line-through' : '' }}">{{ $loop->iteration. ': '.$task->task }}</td>
                                        <td class="{{ $task->is_done ? "text-success" : "text-danger" }} font-bold">{{ $task->is_done ? 'YES' : 'NO' }}</td>
                                        <td>{{ $task->created_at }}</td>
                                        @if(Auth::user()->role->name == "admin")
                                            @if($task->is_done && !$task->confirm)
                                                <td class="flex justify-around">
                                                    <form action="{{ route('tasklist.status.confirm') }}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="confirm" value="1" required>
                                                        <input type="hidden" name="task" value="{{ $task->id }}" required>
                                                        <button type="submit" class="underline text-green-400 hover:text-green-600 font-bold">Confirmed</button>
                                                    </form>
                                                    <form action="{{ route('tasklist.status.reject') }}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="confirm" value="0" required>
                                                        <input type="hidden" name="task" value="{{ $task->id }}" required>
                                                        <button type="submit" class="underline text-red-400 hover:text-red-600 font-bold">Reject</button>
                                                    </form>
                                                </td>
                                            @elseif ($task->is_done && $task->confirm)
                                                <td class="text-uppercase text-green-600 font-bold">Task DONE</td>
                                            @else
                                                <td class="text-uppercase text-yellow-600 font-bold">Task Not Completed Yet</td>
                                            @endif
                                        @endif

                                        @if (Auth::user()->role->name == "saller")
                                            @if($task->is_done)
                                                <td class="text-uppercase text-success font-bold">Comepleted Task</td>
                                            @else
                                                <td>
                                                    <form action="{{ route('tasklist.status.done') }}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="confirm" value="1" required>
                                                        <input type="hidden" name="task" value="{{ $task->id }}" required>
                                                        <button type="submit" class="underline text-green-400 hover:text-green-600 font-bold">Check is Done</button>
                                                    </form>
                                                </td>
                                            @endif
                                        @endif
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</x-panel-layout>
