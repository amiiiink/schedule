@extends("layouts.front.layout-front")

@section('content')
    <main id="main">
        <!--header main page-->
        <x-front.main-slider />

        <!-- pricing-->
        <x-front.pricing-card />

        <!-- customers-->
        <x-front.customer-comment />

        <!-- CRM-->
        <x-front.interduce-crm />

        <!-- brand and news -->
        <x-front.brand-news />

        <!-- demo-->
        <x-front.banner-free-demo />

    </main>
    <!-- End #main -->
@endsection
