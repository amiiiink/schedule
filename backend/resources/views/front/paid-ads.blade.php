<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Specialty ads</title>

    {{-- Styles --}}
    <link rel="stylesheet" href="/front/ads/main.css" />
    <style>
        .float-container {
            padding: 2px;
        }

        .float-child {
            width: 3%;
            float: left;
            padding: 9px;
        }
        .blue{
            width: 200px;
            margin-top: 14px;
            position: relative;
            top: -11px;
        }



        input[type=checkbox] {
            width: 15px;
            height: 15px;
            vertical-align: bottom;
            position: relative;
            top: 2px;
        }
    </style>
</head>

<body>
    <!-- Modal -info -->
    <div class="container">
        @if($errors->any())
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <div id="modal-3" class="modal" data-modal-effect="slide-top">
            <div class="modal-content">
                <h2 class="fs-title">About Specialty ads</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce convallis consectetur ligula. Morbi
                    dapibus tellus a ipsum sollicitudin aliquet. Phasellus id est lacus. Pellentesque a elementum velit,
                    a tempor nulla. Mauris mauris lectus, tincidunt et purus rhoncus, eleifend convallis turpis. Nunc
                    ullamcorper bibendum diam, vitae tempus dolor hendrerit iaculis. Phasellus tellus elit, feugiat vel
                    mi et, euismod varius augue. Nulla a porttitor sapien. Donec vestibulum ac nisl sed bibendum.
                    Praesent neque ipsum, commodo eget venenatis vel, tempus sit amet ante. Curabitur vel odio eget urna
                    dapibus imperdiet sit amet eget felis. Vestibulum eros velit, posuere a metus eget, aliquam euismod
                    purus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
                </p>
                <input type="button" name="next" class="next action-button modal-close" value="Got it!">
            </div>
        </div>
    </div>
    <div style="text-align: center !important;margin-top: 20px;">
        <a href="{{ route('front.index') }}" style="cursor: pointer;">
            <img src="/front/images/logo.png" alt="ezlead" style="cursor: pointer;">
        </a>
    </div>
    <form class="steps" method="POST" action="{{ route('ads-submit') }}">
        @csrf
        <ul id="progressbar">
            <li class="active">Step One</li>
            <li>Step Two</li>
            <li>Step Three</li>
            <li>Step Four</li>
            <li>Step Five</li>
        </ul>

        <!-- USER INFORMATION FIELD SET -->
        <fieldset>
            <!-- Begin What's Your social Field -->
            <div class="hs_social field hs-form-field">

                <label for="social">Is your dealership already active on Social Media? If so on which Platforms</label>

                <div class="float-container">
                    <div class="float-child">
                        <div class="green">
                            <input type="checkbox" name="social[]" value="Instagram">
                            <input type="checkbox" name="social[]" value="Facebook">
                            <input type="checkbox" name="social[]" value="Linkedin">
                            <input type="checkbox" name="social[]" value="Snapchat">
                        </div>
                    </div>
                    <div class="float-child">
                        <div class="blue">Instagram</div>
                        <div class="blue">Facebook</div>
                        <div class="blue">Linkedin</div>
                        <div class="blue">Snapchat</div>
                    </div>
                </div>



                <textarea name="social_extra" id="social-extra" rows="5" placeholder="extra info"></textarea>

                <span class="error1" style="display: none;">
                    <i class="error-log fa fa-exclamation-triangle"></i>
                </span>
            </div>
            <!-- End What's Your social Field -->

            <!-- Begin What's Your achieve Field -->
            <div class="hs_achieve field hs-form-field">
                <label for="achieve">Exposure hoping to achieve from Marketing? </label>
                <div class="float-container">

                    <div class="float-child">
                        <div class="green">
                            <input type="checkbox" name="achieve[]" value="website-traffic">
                            <input type="checkbox" name="achieve[]" value="calls">
                            <input type="checkbox" name="achieve[]" value="lead-generation">
                            <input type="checkbox" name="achieve[]" value="brand-awarness">
                            <input type="checkbox" name="achieve[]" value="messages">
                        </div>
                    </div>
                    <div class="float-child">
                        <div class="blue">Website Traffic</div>
                        <div class="blue">Calls</div>
                        <div class="blue">Lead Generation</div>
                        <div class="blue">Brand Awareness</div>
                        <div class="blue">Messages</div>
                    </div>
                </div>
                <textarea name="achieve_extra" id="achieve-extra" rows="5" placeholder="extra info"></textarea>
                <span class="error1" style="display: none;">
                    <i class="error-log fa fa-exclamation-triangle"></i>
                </span>
            </div>
            <!-- End What's Your achieve Field -->

            <input type="button" data-page="1" name="next" class="next action-button" value="Next" />
            <div class="explanation btn btn-small modal-trigger" data-modal-id="modal-3"><i
                    class="question-log fa fa-question-circle"></i> What Is This?</div>
        </fieldset>

        <!-- ACQUISITION FIELD SET -->
        <fieldset>
            <!-- Begin Total Number of Donors in Year 1 Field -->
            <div class="form-item webform-component webform-component-textfield hs_vehicle_type field hs-form-field"
                id="webform-component-acquisition--amount-1">

                <label for="vehicle_type">Vehicle type Sold at Dealership *</label>
                <div class="float-container">
                    <div class="float-child">
                        <div class="green">
                            <input type="checkbox" name="vehicle_type[]" value="new">
                            <input type="checkbox" name="vehicle_type[]" value="used">
                            <input type="checkbox" name="vehicle_type[]" value="both">
                        </div>
                    </div>
                    <div class="float-child">
                        <div class="blue">New</div>
                        <div class="blue">Both</div>
                        <div class="blue">Used</div>

                    </div>
                </div>
                <textarea name="vehicle_type_extra" id="social-extra" rows="5" placeholder="extra info"></textarea>
                <span class="error1" style="display: none;">
                    <i class="error-log fa fa-exclamation-triangle"></i>
                </span>
            </div>
            <!-- End Total Number of Donors in Year 1 Field -->

            <!-- Begin Total Number of Donors in Year 2 Field -->
            <div class="form-item webform-component webform-component-textfield hs_finance_type field hs-form-field">

                <label for="finance_type">Types of financing at dealership *</label>
                <div class="float-container">
                    <div class="float-child">
                        <div class="green">
                            <input type="checkbox" name="finance_type[]" value="bank-financing">
                            <input type="checkbox" name="finance_type[]" value="in-house-financing">
                        </div>
                    </div>
                    <div class="float-child">
                        <div class="blue">Bank Financing</div>
                        <div class="blue">In House Financing</div>
                    </div>
                </div>
                <textarea name="finance_type_extra" id="social-extra" rows="5" placeholder="extra info"></textarea>
                <span class="error1" style="display: none;">
                    <i class="error-log fa fa-exclamation-triangle"></i>
                </span>
            </div>
            <!-- End Total Number of Donors in Year 2 Field -->
            <input type="button" data-page="2" name="previous" class="previous action-button" value="Previous" />
            <input type="button" data-page="2" name="next" class="next action-button" value="Next" />
            <div class="explanation btn btn-small modal-trigger" data-modal-id="modal-3">What Is This?</div>
        </fieldset>

        <!-- Cultivation FIELD SET -->
        <fieldset>
            <!-- Begin Average Gift Size in Year 1 Field -->
            <div class="form-item webform-component webform-component-textfield hs_target_customer field hs-form-field"
                id="target_customer">
                <label for="target_customer">Target Customers *</label>
                <div class="float-container">
                    <div class="float-child">
                        <div class="green">
                            <input type="checkbox" name="target_customer[]" value="good-credit-only">
                            <input type="checkbox" name="target_customer[]" value="low-credit">
                            <input type="checkbox" name="target_customer[]" value="bilingual">
                        </div>
                    </div>
                    <div class="float-child">
                        <div class="blue">Good Credit only</div>
                        <div class="blue">Low Credit</div>
                        <div class="blue">Bilingual</div>
                    </div>
                </div>

                <textarea name="target_customer_extra" id="social-extra" rows="5" placeholder="extra info"></textarea>
                <span class="error1" style="display: none;">
                    <i class="error-log fa fa-exclamation-triangle"></i>
                </span>
            </div>
            <!-- End Average Gift Size in Year 1 Field -->

            <!-- Begin Average Gift Size in Year 2 Field -->
            <div class="form-item webform-component webform-component-textfield hs_location hs-form-field"
                id="location">

                <label for="location">Geographical Targeting for locations of ads to be placed (If owner does not know
                    we can offer a service where we go through history of sales develop a heat map of where most of
                    sales are coming from to be able to improve targeting) </label>
                <div class="float-container">
                    <div class="float-child">
                        <div class="green">
                            <input type="checkbox" name="location[]" value="city">
                            <input type="checkbox" name="location[]" value="state">
                            <input type="checkbox" name="location[]" value="zip-codes">
                        </div>
                    </div>
                    <div class="float-child">
                        <div class="blue">City</div>
                        <div class="blue">State</div>
                        <div class="blue">Zip Codes</div>
                    </div>
                </div>

                <textarea name="location_extra" id="social-extra" rows="5" placeholder="extra info"></textarea>
                <span class="error1" style="display: none;">
                    <i class="error-log fa fa-exclamation-triangle"></i>
                </span>
            </div>
            <!-- End Average Gift Size in Year 2 Field -->
            <input type="button" data-page="3" name="previous" class="previous action-button" value="Previous" />
            <input type="button" data-page="3" name="next" class="next action-button" value="Next" />
            <div class="explanation btn btn-small modal-trigger" data-modal-id="modal-3">What Is This?</div>
        </fieldset>

        <!-- Cultivation2 FIELD SET -->
        <fieldset>
            <!-- Begin Total Giving In Year 1 Field -->
            <div class="form-item webform-component webform-component-textfield"
                id="hs_vehicle_sell field hs-form-field">
                <label for="vehicle_sell">New VA use bardare </label>
                <div class="float-container">
                    <div class="float-child">
                        <div class="green">
                            <input type="checkbox" name="vehicle_sell[]" value="new">
                            <input type="checkbox" name="vehicle_sell[]" value="used">
                            <input type="checkbox" name="vehicle_sell[]" value="sedans">
                            <input type="checkbox" name="vehicle_sell[]" value="SUVs">
                            <input type="checkbox" name="vehicle_sell[]" value="trucks">
                            <input type="checkbox" name="vehicle_sell[]" value="diesel">
                            <input type="checkbox" name="vehicle_sell[]" value="motorcycles">
                        </div>
                    </div>
                    <div class="float-child">
                        <div class="blue">New</div>
                        <div class="blue">Used</div>
                        <div class="blue">Sedans</div>
                        <div class="blue">SUVs</div>
                        <div class="blue">Trucks</div>
                        <div class="blue">Diesel</div>
                        <div class="blue">Motorcycles</div>
                    </div>
                </div>

                <textarea name="vehicle_sell_extra" id="vehicle_sell_extra" rows="5"
                    placeholder="extra info"></textarea>
                <span class="error1" style="display: none;">
                    <i class="error-log fa fa-exclamation-triangle"></i>
                </span>
            </div>
            <!-- End Total Giving In Year 1 Field -->

            <!-- Begin Total Giving In Year 2 Field -->
            <div class="form-item webform-component webform-component-textfield hs_language field hs-form-field">
                <label for="language">Ads Language </label>
                <div class="float-container">
                    <div class="float-child">
                        <div class="green">
                            <input type="checkbox" name="language[]" value="english">
                            <input type="checkbox" name="language[]" value="spanish">
                            <input type="checkbox" name="language[]" value="both">
                        </div>
                    </div>
                    <div class="float-child">
                        <div class="blue">English</div>
                        <div class="blue">Spanish</div>
                        <div class="blue">Both</div>
                    </div>
                </div>

                <textarea name="language_extra" id="language_extra" rows="5" placeholder="extra info"></textarea>
                <span class="error1" style="display: none;">
                    <i class="error-log fa fa-exclamation-triangle"></i>
                </span>
            </div>
            <!-- End Total Giving In Year 2 Field -->
            <input type="button" data-page="4" name="previous" class="previous action-button" value="Previous" />
            <input type="button" data-page="4" name="next" class="next action-button" value="Next" />
            <div class="explanation btn btn-small modal-trigger" data-modal-id="modal-3">What Is This?</div>
        </fieldset>

        <!-- RETENTION FIELD SET -->
        <fieldset>
            <!-- Begin Total Number of Donors Who Gave in Year 1 Field -->
            <div class="form-item webform-component webform-component-textfield hs_daily_budget field hs-form-field">
                <label for="daily_budget">Specific type of vehicle hoping to sell from Paid Ads </label>
                <div class="float-container">
                    <div class="float-child">
                        <div class="green">
                            <input type="checkbox" name="daily_budget[]" value="new">
                            <input type="checkbox" name="daily_budget[]" value="used">
                            <input type="checkbox" name="daily_budget[]" value="sedans">
                            <input type="checkbox" name="daily_budget[]" value="SUVs">
                            <input type="checkbox" name="daily_budget[]" value="trucks">
                            <input type="checkbox" name="daily_budget[]" value="diesel">
                            <input type="checkbox" name="daily_budget[]" value="motorcycles">
                        </div>
                    </div>
                    <div class="float-child">
                        <div class="blue">New</div>
                        <div class="blue">Used</div>
                        <div class="blue">Sedans</div>
                        <div class="blue">SUVs</div>
                        <div class="blue">Trucks</div>
                        <div class="blue">Diesel</div>
                        <div class="blue">Motorcycles</div>
                    </div>
                </div>

                <textarea name="daily_budget_extra" id="daily_budget_extra" rows="5"
                    placeholder="extra info"></textarea>
                <span class="error1" style="display: none;">
                    <i class="error-log fa fa-exclamation-triangle"></i>
                </span>
            </div>
            <!-- End Total Number of Donors Who Gave in Year 1 Field-->


            <!-- Begin Total Number of Donors Who Gave in Year 1 and Year 2 Field -->
            <div class="form-item webform-component webform-component-textfield"
                id="webform-component-retention--amount-2 hs_number_of_year_1_donors_who_also_gave_in_year_2 field hs-form-field">
                <label for="extra_comment">Additional Comments or Requests *</label>
                <textarea name="extra_comment" id="extra_comment" rows="5" placeholder="extra info"></textarea>
            </div>
            <!-- End Total Number of Donors Who Gave in Year 1 and Year 2 Field -->
            <input type="button" data-page="5" name="previous" class="previous action-button" value="Previous" />
            <input id="submit" class="hs-button primary large action-button next" type="submit" value="Submit">
            <div class="explanation btn btn-small modal-trigger" data-modal-id="modal-3">What Is This?</div>
        </fieldset>
    </form>
    {{-- SCript --}}
    <script src="/front/js/jquery.min.js"></script>
    <script src="/front/js/jquery.easing.min.js"></script>
    <script src="/front/js/jquery.validate.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="/front/ads/main.js"></script>
</body>

</html>
