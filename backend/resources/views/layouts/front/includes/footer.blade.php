<!-- footer-->
<div id="footer">
    <div class="container">
        <img src="/front/images/logo.png" alt="ezlead">
        <div class="row mt-5">
            <div class="col-md-2">
                <div class="ul-footer">
                    <p>TRIALOG</p>
                    <ul>
                        <li><a href="#">About us</a></li>
                        <li class="new-li-footer"><a href="#">Careers</a></li>
                        <li><a href="#">Customers</a></li>
                        <li><a href="#">Request demo</a></li>
                        <li><a href="#">Pricing</a></li>
                        <li><a href="#">Integrations</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
                <div class="ul-footer">
                    <p>OUR PRODUCTS</p>
                    <ul>
                        <li><a href="#">Pulse</a></li>
                        <li class="new-li-footer"><a href="#">Navigator</a></li>
                        <li><a href="#">Customers</a></li>
                        <li><a href="#">Request demo</a></li>
                        <li><a href="#">Pricing</a></li>
                        <li><a href="#">Integrations</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
                <div class="ul-footer">
                    <p>OUR PRODUCTS</p>
                    <ul>
                        <li><a href="#">Pulse</a></li>
                        <li class="new-li-footer"><a href="#">Navigator</a></li>
                        <li><a href="#">Customers</a></li>
                        <li><a href="#">Request demo</a></li>
                        <li><a href="#">Pricing</a></li>
                        <li><a href="#">Integrations</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
                <div class="ul-footer">
                    <p>OUR PRODUCTS</p>
                    <ul>
                        <li><a href="#">Pulse</a></li>
                        <li class="new-li-footer"><a href="#">Navigator</a></li>
                        <li><a href="#">Customers</a></li>
                        <li><a href="#">Request demo</a></li>
                        <li><a href="#">Pricing</a></li>
                        <li><a href="#">Integrations</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
                <div class="ul-footer">
                    <p>OUR PRODUCTS</p>
                    <ul>
                        <li><a href="#">Pulse</a></li>
                        <li class="new-li-footer"><a href="#">Navigator</a></li>
                        <li><a href="#">Customers</a></li>
                        <li><a href="#">Request demo</a></li>
                        <li><a href="#">Pricing</a></li>
                        <li><a href="#">Integrations</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
                <div class="ul-footer">
                    <p>OUR PRODUCTS</p>
                    <ul>
                        <li><a href="#">Pulse</a></li>
                        <li class="new-li-footer"><a href="#">Navigator</a></li>
                        <li><a href="#">Customers</a></li>
                        <li><a href="#">Request demo</a></li>
                        <li><a href="#">Pricing</a></li>
                        <li><a href="#">Integrations</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--    copyright-->
    <div class="copyright">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-md-8">
                    <ul>
                        <li>© Ezlead 2021 Business. All Rights Reserverd.</li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms and Conditions</a></li>
                    </ul>
                </div>
                <div class="col-md-4 text-center">
                    <div class="dropdown dropdown-lang">
                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                            <img src="/front/images/icons/glob.svg" alt="ezlead">
                            English (United States)
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">French</a>
                            <a class="dropdown-item" href="#">German</a>
                            <a class="dropdown-item" href="#">Russian</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
