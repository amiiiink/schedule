<!-- Header -->
<header id="header" class="fixed-top header-transparent">
    <div class="container d-flex align-items-center position-relative">
        <h1 class="logo mr-auto d-none"><a href="/">Ezlead</a></h1>
        <a href="/" class="logo "><img src="/front/images/logo.png" alt="" class="img-fluid"></a>
        <!--header search-->
        <form class="search-container" action="//llamaswill.tumblr.com/search">
            <input id="search-box" type="text" class="search-box" name="q" placeholder="Search here" />
            <label for="search-box"><span class="fa fa-search search-icon"></span></label>
            <input type="submit" id="search-submit" />
        </form>
        <!--     navbar-->
        <nav class="main-nav d-none d-lg-block">
            <ul>
                <li class=" nav-li-xs"><a href="/login">Sign in</a>
                <li class="nav-li-xs-bb nav-li-xs"><a href="">Get started</a>
                <li><a href="{{ route('ads') }}">Specialty ads</a></li>
                <li><a href="{{ route('login') }}">CRM</a></li>
{{--                 <li class="drop-down"><a href="">Features</a>--}}
{{--                    <ul>--}}
{{--                        <li><a href="#">Drop Down 1</a></li>--}}
{{--                        <li class="drop-down"><a href="#">Drop Down 2</a>--}}
{{--                            <ul>--}}
{{--                                <li><a href="#">Deep Drop Down 1</a></li>--}}
{{--                                <li><a href="#">Deep Drop Down 2</a></li>--}}
{{--                                <li><a href="#">Deep Drop Down 3</a></li>--}}
{{--                                <li><a href="#">Deep Drop Down 4</a></li>--}}
{{--                                <li><a href="#">Deep Drop Down 5</a></li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        <li><a href="#">Drop Down 3</a></li>--}}
{{--                        <li><a href="#">Drop Down 4</a></li>--}}
{{--                        <li><a href="#">Drop Down 5</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li><a href="#pricing">Pricing</a></li>--}}
{{--                <li><a href="#customers">Customers</a></li>--}}
{{--                <li><a href="#blog">Blog</a></li>--}}
{{--                <li><a href="#">Help</a></li> --}}
{{--            </ul>--}}
        </nav><!-- .main-nav-->
        <!--login header-->
        <div class="header-login">
            <a href="/login">Sign in</a>
            <a href="#">Get started
                <img src="/front/images/icons/right-arrow.png" />
            </a>
        </div>
    </div>
</header>
<!-- End Header -->
