<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Ezlead</title>
    <meta content="description" name="Ezlead">
    <meta content="" name="Ezlead">

    <!-- Favicons -->
    <link href="" rel="icon">
    <link href="" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="/front/css/bootstrap.min.css" rel="stylesheet">
    <link href="/front/css/font-awesome.min.css" rel="stylesheet">
    <link href="/front/css/owl.carousel.min.css" rel="stylesheet">
    <link href="/front/css/animate.css" rel="stylesheet" media="(min-width:700px)">

    <!-- Template Main CSS File -->
    <link href="/front/css/main.css" rel="stylesheet">
    <link href="/front/css/index.css" rel="stylesheet">

</head>
<body>
    @include('layouts.front.includes.header')
    @yield('content')
    @include('layouts.front.includes.footer-banner')
    @include('layouts.front.includes.footer')

<!-- Vendor JS Files -->
<script src="/front/js/jquery.min.js"></script>
<script src="/front/js/bootstrap.bundle.min.js"></script>
<script src="/front/js/jquery.easing.min.js"></script>
<script src="/front/js/PageScroll2id.js"></script>
<script src="/front/js/owl.carousel.min.js"></script>
<script src="/front/js/animate.js"></script>

<!-- Template Main JS File -->
<script src="/front/js/main.js"></script>
<script src="/front/js/index.js"></script>

</body>

</html>
