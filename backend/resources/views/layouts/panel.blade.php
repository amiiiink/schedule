<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'EzLead') }}</title>

        <link rel="shortcut icon" href="/panel/assets/images/ezlead-logo.png" />

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="/panel/assets/css/bootstrap.css">
        <link rel="stylesheet" href="/libs/flatpickr/dist/flatpickr.min.css">
        <link rel="stylesheet" href="/panel/assets/vendors/chartjs/Chart.min.css">
        <link rel="stylesheet" href="/panel/assets/vendors/perfect-scrollbar/perfect-scrollbar.css">
        <link rel="stylesheet" href="/panel/assets/vendors/choices.js/choices.min.css" />
        <link rel="stylesheet" href="/panel/assets/css/app.css">
        <link rel="stylesheet" href="/panel/assets/css/base.css">


        {{-- extra css --}}
        {{ isset($links) ? $links : '' }}

        @livewireStyles
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="app">
            <x-shared.sidebar logo='/front/images/logo.png' />
            <div id="main">
                <x-shared.header />

                {{-- Message --}}
                @if (session()->has('message'))
                    <div class="alert alert-info">
                        {{ session('message') }}
                    </div>
                @endif

                {{-- Message --}}
                @if (session()->has('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                {{-- Message --}}
                @if (session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                <div class="main-content container-fluid">
                    @isset($title)
                        {{ $title }}
                    @else
                        <x-shared.page-title />
                    @endisset
                    {{ $slot }}
                </div>
                <x-shared.footer />
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modal" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        {{ isset($modal) ? $modal : 'emtpy' }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        {{-- SCRIPT --}}
        <script src="/libs/jquery/jquery.min.js"></script>
        <script src="/libs/flatpickr/dist/flatpickr.min.js"></script>
        <script src="/panel/assets/js/feather-icons/feather.min.js"></script>
        <script src="/panel/assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
        <script src="/panel/assets/js/app.js"></script>
        <script src="/panel/assets/vendors/chartjs/Chart.min.js"></script>
        <script src="/panel/assets/vendors/apexcharts/apexcharts.min.js"></script>
        <script src="/panel/assets/vendors/choices.js/choices.min.js"></script>
        <script src="/panel/assets/js/pages/dashboard.js"></script>
        <script src="{{ asset('js/app.js') }}"></script>
        {{-- Base js --}}
        <script src="/panel/assets/js/main.js"></script>

        {{-- extra js --}}
        {{ isset($scripts) ? $scripts : '' }}

        @livewireScripts
    </body>
</html>
