<section class="section">
    {{-- <div id="delivered" class="card">
        <div class="card-body table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr class="bg-green-500 text-white">
                        <th colspan="4" class="text-capitalize rounded">5 delivered</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <x-badge.tag color="lightblue" text="delivered" />
                            <span class="ml-3">Customer name</span>
                        </td>
                        <td>Vehicle Name and Time delivered</td>
                        <td>Dealer</td>
                        <td>Time In Current Day</td>
                    </tr>
                    <tr>
                        <td>
                            <x-badge.tag color="lightblue" text="delivered" />
                            <span class="ml-3">Customer name</span>
                        </td>
                        <td>Vehicle Name and Time delivered</td>
                        <td>Dealer</td>
                        <td>Time In Current Day</td>
                    </tr>
                    <tr>
                        <td>
                            <x-badge.tag color="lightblue" text="delivered" />
                            <span class="ml-3">Customer name</span>
                        </td>
                        <td>Vehicle Name and Time delivered</td>
                        <td>Dealer</td>
                        <td>Time In Current Day</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div id="sold" class="card">
        <div class="card-body table-responsive">
            <table class="table table-hover ">
                <thead>
                    <tr class="bg-gray-200">
                        <th colspan="4" class="text-capitalize rounded">5 sold</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <x-badge.tag color="lightblue" text="delivered" />
                            <span class="ml-3">Customer name</span>
                        </td>
                        <td>Vehicle Name and Time delivered</td>
                        <td>Dealer</td>
                        <td>Time In Current Day</td>
                    </tr>
                    <tr>
                        <td>
                            <x-badge.tag color="lightblue" text="delivered" />
                            <span class="ml-3">Customer name</span>
                        </td>
                        <td>Vehicle Name and Time delivered</td>
                        <td>Dealer</td>
                        <td>Time In Current Day</td>
                    </tr>
                    <tr>
                        <td>
                            <x-badge.tag color="lightblue" text="delivered" />
                            <span class="ml-3">Customer name</span>
                        </td>
                        <td>Vehicle Name and Time delivered</td>
                        <td>Dealer</td>
                        <td>Time In Current Day</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div> --}}
    <div id="upsheet" class="card">
        <div class="card-body table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr class="bg-yellow-300 text-white">
                        <th colspan="4" class="text-capitalize rounded">{{ $upsheets->count() }} New Up Sheets</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($upsheets as $upsheet)
                        <tr>
                            <td>
                                <i class="text-danger" data-feather="user-plus"></i>
                                <span class="ml-2">{{ $upsheet->customer->firstname . ' ' . $upsheet->customer->lastname }}</span>
                            </td>
                            <td>{{ $upsheet->user->firstname. ' ' . $upsheet->user->lastname }}</td>
                            <td class="text-center">
                                <span class="bg-yellow-300 text-white font-semibold px-4 py-1 inline-block shadow-sm rounded-2xl">
                                    {{ $upsheet->created_at->format('Y M d - H:i') }}
                                </span>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>

<x-slot name="links">

</x-slot>

<x-slot name="scripts">

</x-slot>
