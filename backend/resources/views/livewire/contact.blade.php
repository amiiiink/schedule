<section class="section">
    <div class="row">
        <div class="col-md-12">
            <div class="card rounded-lg">
                <div class="card-header">
                    <a href="{{ route('panel.contacts.create') }}" class="btn btn-success btn-lg">New Contact</a>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr class="bg-yellow-300 text-white">
                                <th>FirstName</th>
                                <th>LastName</th>
                                <th>Email</th>
                                <th>Mobile</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($customers as $customer)
                                <tr>
                                    <td>{{ $customer->firstname }}</td>
                                    <td>{{ $customer->lastname }}</td>
                                    <td>{{ $customer->email  ? $customer->email : 'empty' }}</td>
                                    <td>{{ $customer->mobile ? $customer->mobile : 'empty' }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    {{ $customers->links() }}
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card rounded-lg">
                <div class="card-body  table-responsive">
                    <table class="table">
                        <thead>
                            <tr class="bg-gray-200">
                                <th colspan="4" class="text-capitalize rounded">3 Important Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <x-badge.tag color="red" text="important date" />
                                    <span class="ml-3">Customer name</span>
                                </td>
                                <td>Vehicle Name and Time delivered</td>
                                <td>Dealer</td>
                                <td>Time In Current Day</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<x-slot name="links">

</x-slot>

<x-slot name="scripts">

</x-slot>
