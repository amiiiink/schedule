<form wire:submit.prevent="addAppointment" class="form form-vertical">
    @csrf
    <div class="form-body">
        <div class="row">
            <input type="hidden" value="{{ $upsheet_id }}" name="customer_id" wire:model="customer_id" />
            <input type="hidden" value="{{ $customer_id }}" name="upsheet_id" wire:model="upsheet_id" />
            <div class="col-12">
                <div class="form-group has-icon-left">
                    <label for="mobile-id-icon">Schedule DateTime <span class="text-danger">*</span></label>
                    <div class="position-relative">
                        <input wire:model="schedule" name="schedule" type="text" class="form-control" placeholder="Schedule" id="schedule-appointment" autocomplete="off">
                        <div class="form-control-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar3-week" viewBox="0 0 16 16">
                                <path d="M14 0H2a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"/>
                                <path d="M12 7a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-5 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm2-3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-5 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                            </svg>
                        </div>
                    </div>
                    @error('schedule')
                        <span class="text-red-500 block my-2">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="col-12 d-flex justify-content-end">
                <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                <button type="reset" class="btn btn-light-secondary mr-1 mb-1">Reset</button>
            </div>
        </div>
    </div>
</form>
