<form wire:submit.prevent class="form form-vertical" id="status-form" method="POST">
    @csrf
    @method('UPDATE')
    <select
        wire:model="statusId"
        class="form-select text-capitalize"
        wire:change="changeUpsheetStatus"
    >
        @foreach($status as $item)
            @if ($upsheet->status->id == $item->id)
                <option value="{{ $item->id }}" selected>{{ $item->name }}</option>
                @continue
            @endif
            <option value="{{ $item->id }}">{{ $item->name }}</option>
        @endforeach
    </select>
    @error('statusId')
        <span class="text-red-500 block my-2">{{ $message }}</span>
    @enderror
</form>
