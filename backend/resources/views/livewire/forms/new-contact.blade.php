<section class="section">
    <div class="row justify-content-center">
        <div class="col-9">
            <div class="card">
                <div class="card-header" flex>
                    <div class="row">
                        <div class="col-6">
                            <h3 class="card-title">Add Contact</h3>
                        </div>
                        <div class="col-6 text-right">
                            <a class="btn btn-secondary" href="{{ route('dashboard') }}">Go To Dashboard</a>
                        </div>
                    </div>
                </div>
                <div class="card-content">
                <div class="card-body">
                    <form wire:submit.prevent="saveContact" class="form form-vertical">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <label for="first-name-icon">First Name <span class="text-danger">*</span></label>
                                        <div class="position-relative">
                                            <input wire:model="firstname" name="firstname" type="text" class="form-control" placeholder="your first name" id="first-name-icon" focus />
                                            <div class="form-control-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                            </div>
                                        </div>
                                        @error('firstname')
                                            <span class="text-red-500 block my-2">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <label for="first-name-icon">Last Name <span class="text-danger">*</span></label>
                                        <div class="position-relative">
                                            <input wire:model="lastname" name="lastname" type="text" class="form-control" placeholder="your last name" id="first-name-icon">
                                            <div class="form-control-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                            </div>
                                        </div>
                                        @error('lastname')
                                            <span class="text-red-500 block my-2">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <label for="email-id-icon">Email <span class="text-danger">*</span></label>
                                        <div class="position-relative">
                                            <input wire:model="email" name="email" type="text" class="form-control" placeholder="Email" id="email-id-icon">
                                            <div class="form-control-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                                            </div>
                                        </div>
                                        @error('email')
                                            <span class="text-red-500 block my-2">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <label for="mobile-id-icon">Mobile <span class="text-danger">*</span></label>
                                        <div class="position-relative">
                                            <input wire:model="mobile" name="mobile" type="text" class="form-control" placeholder="Mobile" id="mobile-id-icon">
                                            <div class="form-control-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone"><path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg>
                                            </div>
                                        </div>
                                        @error('mobile')
                                            <span class="text-red-500 block my-2">{{ $message }}</span>
                                        @enderror
                                        <small>example: (333)555-9999</small>
                                    </div>
                                </div>
                                <div class="col-12 d-flex justify-content-end">
                                    <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                                    <button type="reset" class="btn btn-light-secondary mr-1 mb-1">Reset</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</section>

<x-slot name="links">

</x-slot>

<x-slot name="scripts">

</x-slot>
