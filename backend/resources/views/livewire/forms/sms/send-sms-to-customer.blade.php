<form wire:submit.prevent="sendMessageToCustomer" class="form mb-3">
    @if(session()->has('error'))
        <div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Error!</strong> {{ session('error') }}
        </div>
    @endif
    @error('message')
        <span class="text-red-500 block my-2">{{ $message }}</span>
    @enderror
    @error('type')
        <span class="text-red-500 block my-2">{{ $message }}</span>
    @enderror
    <div class="form-group flex">
        <div class="form-check">
            <input wire:model="type" class="form-check-input" type="radio" id="sms" value="sms">
            <label class="form-check-label" for="sms">
              SMS
            </label>
        </div>
        <div class="form-check ml-4">
            <input wire:model="type" class="form-check-input" type="radio" id="call" value="call">
            <label class="form-check-label" for="call">
                Call
            </label>
        </div>
    </div>
    <div class="form-group with-title mb-3">
        <textarea wire:model="message" name="message" class="form-control" id="upsheet-message" rows="3"></textarea>
        <label>message sended to customer</label>
    </div>
    <button type="submit" class="bg-yellow-300 hover:bg-yellow-600 px-4 py-1 rounded-md text-white mr-1 mb-1 font-semibold">Send</button>
</form>
