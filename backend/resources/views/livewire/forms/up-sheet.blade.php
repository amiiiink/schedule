<section class="section">
    <div class="row justify-content-center">
        @if( session()->has('customer') )
        <div class="col-9">
            <div class="card table-responsive">
                <div class="card-header">
                    <h3 class="font-bold underline">This User `{{ session('customer')->firstname.' '.session('customer')->lastname }}` already exist</h3>
                </div>
                <div class="card-body">
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Contact</th>
                                <th>Created By</th>
                                <th>Upsheet</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach (session('customer')->upsheets as $upsheet)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td class="capitalize">{{ $upsheet->customer->firstname. ' '. $upsheet->customer->lastname }}</td>
                                    <td class="capitalize">{{ $upsheet->user->firstname. ' '. $upsheet->user->lastname }}</td>
                                    <td>
                                        <span class="badge mr-2 font-semibold" style="background-color: {{ $upsheet->status->color }}">{{ $upsheet->status->name }}</span>
                                    </td>
                                    <td>
                                        <a href="{{ route('upsheets.show', $upsheet->id) }}" class="capitalize underline text-blue-500 hover:text-blue-900 cursor-pointer">Show/Update This Up-Sheet</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="form-box flex justify-content-end p-3 border-t-2 border-Gray-200">
                        <form wire:submit.prevent="saveNewUpsheet" class="form form-vertical">
                            @csrf
                            <button type="submit" class="capitalize underline text-green-500 hover:text-green-900 cursor-pointer">None of above and create new upsheet</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="col-9">
            <div class="card">
                <div class="card-header" flex>
                    <div class="row">
                        <div class="col-6">
                            <h3 class="card-title">Add Contact for new Up Sheet</h3>
                        </div>
                        <div class="col-6 text-right">
                            <a class="btn btn-secondary" href="{{ route('dashboard') }}">Go To Dashboard</a>
                        </div>
                    </div>
                </div>
                <div class="card-content">
                <div class="card-body">
                    <form wire:submit.prevent="saveUpsheet" class="form form-vertical">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <label for="first-name-icon">title <span class="text-danger">*</span></label>
                                        <div class="position-relative">
                                            <input wire:model="title" name="firstname" type="text" class="form-control" placeholder="Title" id="first-name-icon" focus />
                                            <div class="form-control-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                            </div>
                                        </div>
                                        @error('title') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <label for="first-name-icon">Description <span class="text-danger">*</span></label>
                                        <div class="position-relative">
                                            <textarea wire:model="description" class="form-control" placeholder="Description" id="first-name-icon"></textarea>

                                        </div>
                                        @error('description') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                </div>


                                <div class="col-12 d-flex justify-content-end">
                                    <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                                    <button type="reset" class="btn btn-light-secondary mr-1 mb-1">Reset</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</section>

<x-slot name="links">

</x-slot>

<x-slot name="scripts">

</x-slot>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(function () {

        $('#mobile-id-icon').keydown(function (e) {
            var key = e.charCode || e.keyCode || 0;
            $text = $(this);
            if (key !== 8 && key !== 9) {
                if ($text.val().length === 3) {
                    $text.val($text.val() + '-');
                }
                if ($text.val().length === 7) {
                    $text.val($text.val() + '-');
                }

            }

            return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
        })
    });
</script>
