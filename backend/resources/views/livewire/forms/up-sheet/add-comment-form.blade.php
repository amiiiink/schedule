<form wire:submit.prevent="addNewCommentForUpsheet" class="form">
    <div class="form-group with-title mb-3">
        <textarea wire:model="message" name="message" class="form-control" id="upsheet-message" rows="3"></textarea>
        <label>your message</label>
    </div>
    <button type="submit" class="bg-yellow-300 hover:bg-yellow-600 px-4 py-1 rounded-md text-white mr-1 mb-1 font-semibold">Save</button>

    @error('message')
        <span class="text-red-500 block my-2">{{ $message }}</span>
    @enderror
</form>
