<form wire:submit.prevent="DeleteUpSheetComment" class="inline">
    @csrf
    @method('DELETE')
    <button type="submit" class="underline text-red-600 hover:text-red-700">Delete</button>
</form>
