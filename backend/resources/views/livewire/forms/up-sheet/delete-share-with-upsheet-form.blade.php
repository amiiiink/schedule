<form wire:submit.prevent="deleteShareWith" class="inline">
    @csrf
    <button type="submit" class="px-3 py-1 text-red-500 hover:text-red-700 underline">Delete</button>
</form>
