<button type="button" class="btn btn-danger text-capitalize px-2 py-1 font-bold" onclick="removeUpsheetJs({{ $upsheetId }})">Delete Upsheet</button>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function removeUpsheetJs(id){

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {

            if (result.isConfirmed) {

                window.livewire.emit("removeUpSheet",id);
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            }
        })
    }

</script>
