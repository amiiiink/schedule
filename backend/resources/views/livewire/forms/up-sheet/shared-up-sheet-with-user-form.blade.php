<form  wire:submit.prevent="addSharedUserToUpsheet" class="form form-vertical">
    @csrf
    <div class="col-12">
        <div class="form-group has-icon-left">
            <label for="mobile-id-icon">choose a user want share with:</label>
            <div class="position-relative">
                <select wire:model.defer="userId" name="userId" id="userId" class="form-select mb-1">
                    <option value="none" selected>Choose</option>
                    @foreach($dealers as $dealer)
                        <option value="{{ $dealer->id }}">{{ $dealer->firstname.' '.$dealer->lastname }}</option>
                    @endforeach
                </select>
                <button type="submit" class="bg-yellow-300 hover:bg-yellow-600 px-4 py-1 rounded-md text-white mr-1 mb-1 font-semibold">SAVE</button>
            </div>
            @error('userId')
                <span class="text-red-500 block my-2">{{ $message }}</span>
            @enderror
        </div>
    </div>
</form>
