<section class="section">
    <div class="row mb-2">
        <div class="card">
            <div class="card-body">
                <div id="calendarWeaks"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div id="calendarDays"></div>
            </div>
        </div>
    </div>
</section>

<x-slot name="links">
    <link href="/libs/fullcalendar/main.min.css" rel="stylesheet" />
</x-slot>

<x-slot name="scripts">

    <script src="/libs/fullcalendar/main.min.js"></script>
    <script>
        $(document).ready(() => {
            // calendar instance
            let calendarEl = document.getElementById('calendarWeaks');
            let calendar = new FullCalendar.Calendar(calendarEl, {
                initialView: 'dayGridWeek',
                height: 400,
                nowIndicator: true,
                eventOrder: "-start,-duration",
                droppable: false,
                editable: false,
                events: [
                    @foreach($appointments as $item)
                    {
                        id: "{{ $item->up_sheet_id }}",
                        title: "{{ $item->reminder->format('H:i A') }}",
                        start: "{{ $item->reminder->format('Y-m-d') }}",
                        backgroundColor: "{{ $item->reminder->isPast() ? 'darkred' : 'darkgreen' }}",
                        borderColor: "{{ $item->reminder->isPast() ? 'darkred' : 'darkgreen' }}",
                        classNames: "d-inline-block px-1 font-semibold",
                        url: `/dashboard/upsheets/{{ $item->up_sheet_id }}`
                    },
                    @endforeach
                ]
            });
            calendar.render();

            // calendar instance two
            let calendarEl2 = document.getElementById('calendarDays');
            let calendar2 = new FullCalendar.Calendar(calendarEl2, {
                initialView: 'listWeek',
                eventOrder: "-start,-duration",
                droppable: false,
                editable: false,
                events: [
                        @foreach($appointments as $item)
                    {
                        id: "{{ $item->up_sheet_id }}",
                        title: "{{ $item->reminder->format('H:i A') }}",
                        start: "{{ $item->reminder->format('Y-m-d') }}",
                        backgroundColor: "{{ $item->reminder->isPast() ? 'darkred' : 'darkgreen' }}",
                        borderColor: "{{ $item->reminder->isPast() ? 'darkred' : 'darkgreen' }}",
                        classNames: "",
                        url: `/dashboard/upsheets/{{ $item->up_sheet_id     }}`,
                        extendedProps: {
                            status: "{{ $item->id }}"
                        }
                    },

                    @endforeach
                ],
                eventDidMount: function(info) {
                    // if (info.event.extendedProps.status === 'active') {

                    //     // Change background color of row
                    //     info.el.style.backgroundColor = 'darkred';

                    //     // Change color of dot marker
                    //     var dotEl = info.el.getElementsByClassName('fc-event-dot')[0];
                    //     if (dotEl) {
                    //         dotEl.style.backgroundColor = 'white';
                    //     }
                    // }
                }
            });
            calendar2.render();
        })
    </script>
</x-slot>
