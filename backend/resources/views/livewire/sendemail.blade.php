<div>
    <div>
        @if (session()->has('successMsg'))
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> {{ session('successMsg') }}
            </div>
        @elseif(session()->has('error'))
            <div class="alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Error!</strong> {{ session('error') }}
            </div>
        @endif

    </div>
    <form class="form" wire:submit.prevent="submit" enctype="multipart/form-data">
        <div class="form-group with-title mb-3">
            <select class="form-control" wire:model="title" name="title" wire:change="changeEvent($event.target.value)">
                    <option value="(Action Plan) 1 Day - Follow Up">(Action Plan) 1 Day - Follow Up</option>
                    <option value="(Action Plan) 10 Day - Trying to Reac...">(Action Plan) 10 Day - Trying to Reac...</option>
                    <option value="(Action Plan) 15 Day - In, Out, Jump">(Action Plan) 15 Day - In, Out, Jump</option>
                    <option value="(Action Plan) 3 Day - No Word Yet">(Action Plan) 3 Day - No Word Yet</option>
            </select>

        </div>


        <div class="form-group with-title mb-3">
            <textarea wire:model="message" name="message" class="form-control" id="upsheet-message" rows="3">{{ $message }}</textarea>
            <label>your message</label>
        </div>

        <div class="form-group">
            <input type="file" class="form-control" wire:model="attaches"    />
        </div>







        <button type="submit" wire:click.prevent="sendEmail" wire:loading.class="bg-gray" wire:loading.attr="disabled" class="bg-yellow-300 hover:bg-yellow-600 px-4 py-1 rounded-md text-white mr-1 mb-1 font-semibold">Send</button>


        <div wire:loading wire:target="sendEmail">
            please wait sending email...
        </div>

        @error('message')
        <span class="text-red-500 block my-2">{{ $message }}</span>
        @enderror
    </form>

</div>
