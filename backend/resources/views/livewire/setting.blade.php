<section class="section">
    <form  wire:submit.prevent="dealershipDetailsEdit" class="dealership-details">
        @csrf
        @method('UPDATE')
        {{-- Settings --}}
        <div class="card">
            <div class="card-header">
                <h3 class="font-semibold border-b-2 pb-1">Setting</h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="basicInput" class="capitalize">dealership name</label>
                    <input wire:model="name" type="text" class="form-control" id="basicInput" placeholder="Name" value="{{ $dealership[0]->name }}">
                </div>
                <div class="form-group">
                    <label for="basicInput" class="capitalize">timezone</label>
                    <input wire:model="timezone" type="text" class="form-control" id="basicInput" placeholder="timezone" value="{{ $dealership[0]->timezone }}" disabled>
                </div>
            </div>
        </div>

        {{-- Contact --}}
        <div class="card">
            <div class="card-header">
                <h3 class="font-semibold border-b-2 pb-1">Contact</h3>
            </div>
            <div class="card-body">
                @csrf
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="basicInput" class="capitalize">website</label>
                            <input wire:model="website" type="url" class="form-control" id="basicInput" placeholder="dealership website">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="basicInput" class="capitalize">dealership phone number</label>
                            <input wire:model="phone_number" type="number" class="form-control" id="basicInput" placeholder="phone number">
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <div class="form-group">
                            <label for="basicInput" class="capitalize">address</label>
                            <input wire:model="street_1" type="text" class="form-control" id="basicInput" placeholder="street line 1">
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="form-group">
                            <label for="basicInput" class="capitalize"></label>
                            <input wire:model="street_2" type="text" class="form-control" id="basicInput" placeholder="street line 2">
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-12">
                        <div class="form-group">
                            <label for="basicInput" class="capitalize"></label>
                            <input wire:model="street_3" type="text" class="form-control" id="basicInput" placeholder="street line 1">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="form-group">
                            <label for="basicInput" class="capitalize"></label>
                            <input wire:model="street_4" type="text" class="form-control" id="basicInput" placeholder="street line 2">
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-12">
                        <div class="form-group">
                            <label for="basicInput" class="capitalize"></label>
                            <input wire:model="street_5" type="text" class="form-control" id="basicInput" placeholder="street line 1">
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12">
                        <div class="form-group">
                            <label for="basicInput" class="capitalize"></label>
                            <select wire:model="country" name="country" id="country" class="form-select" disabled>
                                <option value="1" selected>United State</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Configurations --}}
        <div class="card">
            <div class="card-header">
                <h3 class="font-semibold border-b-2 pb-1">Configurations</h3>
            </div>
            <div class="card-body">
                @csrf
                <div class="form-group my-3">
                    <h6 class="capitalize">task scheduling</h6>
                    <input wire:model="close" type="checkbox" class="form-check-input" id="basicInput" placeholder="Enter email" />
                    <label for="basicInput" style="text-transform:none;">This dealership is closed on sunday, do not schedule action plans tasks on sundays.</label>
                </div>
                <div class="form-group my-3">
                    <label for="basicInput" class="capitalize">birthday email template</label>
                    <select wire:model="country" name="country" id="country" class="form-select" disabled>
                        <option value="1" selected>(SPECIAL) Birthdat Email</option>
                    </select>
                    <p class="my-3"><span class="font-semibold">Note:</span> you can only pick templates with 'Birthday' in the name here.</p>
                </div>
                <div class="form-group my-3">
                    <label for="basicInput" class="capitalize block">Inactive Delay</label>
                    After&nbsp;<input wire:model="inactive" type="number" id="basicInput" placeholder="number of day" class="p-1 inline-block focus:ring-yellow-500 focus:border-yellow-500 rounded-md">&nbsp;days of inactivity an up sheet will be marked 'inactive' automatically
                </div>
            </div>
        </div>

        {{-- Permissions --}}
        <div class="card">
            <div class="card-header">
                <h3 class="font-semibold border-b-2 pb-1">Permissions</h3>
            </div>
            <div class="card-body">
                @csrf
                <div class="form-group">
                    <label for="basicInput" class="capitalize">Access to data</label>
                    <div class="form-check my-2">
                        <input wire:model="access_data" value="1" class="form-check-input" type="radio" name="access_data" id="access_data1">
                        <label class="form-check-label" for="flexRadioDefault1">
                            - Only assigned or sharing users may view and edit contacts and up sheets
                        </label>
                    </div>
                    <div class="form-check my-2">
                        <input wire:model="access_data" value="2" class="form-check-input" type="radio" name="access_data" id="access_data2">
                        <label class="form-check-label" for="flexRadioDefault2">
                            - Anyone may view and edit contacts and upsheets
                        </label>
                    </div>
                    <p><span class="font-semibold my-3">Note:</span> Giving a permission here will give that permission to  every user at dealership</p>
                    <div class="form-check my-2">
                        <div class="checkbox">
                            <input wire:model="user_can_see_report" type="checkbox" id="checkbox1" class="form-check-input">
                            <label for="checkbox1">Allow Sales Reps to view dealership reports</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Save settings --}}
        <button type="submit" class="px-4 py-3 bg-gray-800 hover:bg-gray-900 rounded-md text-white w-1/6 hover:shadow-lg">Save</button>
        <div class="mt-2">
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
        </div>
    </form>
</section>

<x-slot name="links"></x-slot>

<x-slot name="scripts"></x-slot>
