<section class="section">
    <div class="row mb-2">
        <div class="col-12 table-responsive">
            <div class="card">
                <div class="card-body table-responsive">
                    <table class="table bg-white">
                        <thead>
                            <tr class="bg-yellow-300 text-white">
                                <th>#</th>
                                <th>title</th>
                                <th>Description</th>
                                <th>see More</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php($i=1)
                        @foreach($jobs as $job)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $job->title }}</td>
                                <td>{{ $job->description }}</td>
                                <td> <a class="capitalize text-lg underline text-blue-900 hover:text-blue-500" href="{{ route('upsheets.show', $job->id) }}">see job</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</section>

<x-slot name="links">

</x-slot>

<x-slot name="scripts">

</x-slot>
