<section class="section">
    <div class="row justify-content-center mb-2 gap-0">
        {{-- return to list --}}
        <div class="col-10 mb-4">
            <a href="/dashboard/upsheets" class="btn btn-primary text-capitalize">back to job list</a>
        </div>

        {{-- Scheduled Messages --}}
        <div class="card-body d-flex justify-content-between align-items-center shadow-none pb-0 mb-3 bg-light-gray" style="border-bottom: solid 1px #444">
            <p class="font-extrabold my-0">Scheduled Messages</p>
            <div class="d-flex">
                <a href="javascript:void(0)" class="text-capitalize" id="toggle-form-schedule">add</a>
            </div>
        </div>
        <div class="card-body table-responsive">
            <div class="form-schedule">
                <livewire:forms.schedule.add-schedule :upsheetId="$job->id" :userId="Auth::user()->id" />
            </div>
            <ul>
                @foreach($job->schedules as $schedule)
                    <li style="padding: 7px;">-&nbsp;
                        <span class="text-capitalize font-semibold">
                            {{ $schedule->user->firstname." ".$schedule->user->lastname }}
                        </span>
                        has requested to do it at
                        <span class="bg-info text-white p-1 rounded">
                            {{ $schedule->reminder->format('Y F d H:i') }}
                        </span>
                        &nbsp;
                         message :
                        <span>
                           {{ $schedule->message }}
                        </span>
                        @if(auth()->user()->is_admin() &&  $schedule->status == 0)
                        <form method="post" action="{{ route("accept") }}">
                            @csrf
                            <input type="hidden" name="id" value="{{ $schedule->id }}">
                            <input type="hidden" name="reminder" value="{{ $schedule->reminder }}">
                            <input type="hidden" name="user_id" value="{{ $schedule->user_id }}">
                            <input type="hidden" name="up_sheet_id" value="{{ $schedule->up_sheet_id }}">
                            <input type="submit" style="width: 100px; " class="btn btn-success btn-xs" value="accept" />
                        </form>
                        @endif
                    </li>
                @if($schedule->status == 1)
                        <span class="badge badge-info" style="background: #db3dc7">accepted</span>
                @else
                        <span class="badge badge-info" style="background: #f50038">Not Accepted</span>
                @endif
                    <hr />
                @endforeach
            </ul>
        </div>


    </div>
</section>

{{-- Modal Appointment --}}
<x-slot name="modal">
    <livewire:forms.appointment-form :upsheetId="$job->id" :customerId="1"/>
</x-slot>

<x-slot name="links"></x-slot>

<x-slot name="scripts">
    <script>
        $('#myModal').on('shown.bs.modal', function () {
            $('#myInput').trigger('focus')
        })

        // hidden or display share-with-form
        $('#choose-share').on('click', (e) => {
            e.preventDefault()
            let display = $('#share-with-form').data('display')
            if (!display) {
                $('#share-with-form').removeClass('hidden')
                $('#share-with-form').addClass('block')
                $('#share-with-form').data('display', true)
                return
            }

            $('#share-with-form').removeClass('block')
            $('#share-with-form').addClass('hidden')
            $('#share-with-form').data('display', false)
        })

        @if(Auth::user()->is_admin())
            // hidden or display ownerchange-form
            $('#owner-chnage').on('click', (e) => {
                e.preventDefault()
                let display = $('#owner-chnage-form').data('display')
                if (!display) {
                    $('#owner-chnage-form').removeClass('hidden')
                    $('#owner-chnage-form').addClass('block')
                    $('#owner-chnage-form').data('display', true)
                    return
                }

                $('#owner-chnage-form').removeClass('block')
                $('#owner-chnage-form').addClass('hidden')
                $('#owner-chnage-form').data('display', false)
            })
        @endif

        $("#schedule, #schedule-appointment").flatpickr({
            enableTime: true,
            time_24hr: true,
            dateFormat: "Y-m-d H:i",
        });

        $('#taggle-form-task').on('click', () => {
            $('.form-task').toggle()
        })

        // $('#taggle-form-sms').on('click', () => {
        //     $('.form-sms').toggle()
        // })

        $('#toggle-form-schedule').on('click', () => {
            $('.form-schedule').toggle()
        })
    </script>
</x-slot>
