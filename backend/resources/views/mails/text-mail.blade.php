@component('mail::message')
# Order Shipped
[]Name: {{ $user->firstname }}]
[]lastname: {{ $user->lastname }}]
(mobile: {{ $user->mobile }})
(email: {{ $user->email }})
[Your order has been shipped(!)]

Thanks,<br>
{{ config('app.name') }}
@endcomponent
