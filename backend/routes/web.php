<?php

use Illuminate\Support\Facades\Route;


Route::middleware(['auth:sanctum', 'verified'])
    ->namespace("\App\Http\Controllers")
    ->group(function() {
        Route::get('/dashboard', "DashboardController@dashboard")->name('dashboard');
    });

// MAIN PAGES Dashboard -----------------------------------------------
Route::middleware(['auth:sanctum', 'verified'])->prefix('dashboard')->group(function () {
    // dashboard Pages

    Route::get("/upsheets", \App\Http\Livewire\UpSheet::class)->name('panel.upsheets');


    // forms
    Route::middleware(['isAdmin'])->group(function(){
        Route::get("/schedules", \App\Http\Livewire\Schedule::class)->name('panel.schedules');
        Route::get("/up-sheets/create", \App\Http\Livewire\Forms\UpSheet::class)->name('panel.upsheets.create');
        Route::post("/up-sheets/store", \App\Http\Livewire\Forms\UpSheet::class)->name('panel.upsheets.store');
        Route::post("/up-sheets/delete", \App\Http\Livewire\Forms\UpSheet::class)->name('panel.upsheets.delete');
    });

    // permissions
    Route::get("/permission", "\App\Http\Controllers\Roles\PermissonController@index")->name('permission.index');
    Route::post("/permission", "\App\Http\Controllers\Roles\PermissonController@store")->name('permission.store');
});

Route::prefix("dashboard/upsheets")->namespace('\App\Http\Livewire')->middleware(['auth:sanctum', 'verified'])->group(function() {
    Route::get("/{upSheetId}", \Upsheet\Show::class)->name('upsheets.show');
});

Route::prefix("dashboard/appointments")->namespace('\App\Http\Livewire')->middleware(['auth:sanctum', 'verified'])->group(function() {
    Route::post("/", \Forms\AppointmentForm::class)->name('appointments.store');
});

Route::post("/accept","\App\Http\Controllers\DashboardController@accept")->name("accept");



// Front End Routes
Route::namespace('\App\Http\Controllers\Front')->group(function() {
    Route::get("/", 'Pages@index')->name("front.index");
});

