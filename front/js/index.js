//top carousel
$('.top-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    dots:true,
    autoplay: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});
//customers icons
$( document ).ready(function() {
    $(".customers-icons .fa").each( function() {
        var randLeft = Math.floor(Math.random() * 90) + 1;
        var randfontSize = Math.floor(Math.random() * 3) + 8;
        var randRotation = Math.floor(Math.random() * 360) + 1;
        var randDelay = Math.floor(Math.random() * 5) + 1;
        var randDuration = Math.floor(Math.random() * 10) + 5;
        $(this).attr("style",("left:" + randLeft + "vw; font-size:" + randfontSize + "vw; transform: rotate(" + randRotation +"deg); animation-delay: " + randDelay + "s; animation-duration: " + randDuration + "s;)"));
    });
});
//news carousel
$('.news-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    dots:true,
    autoplay: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:4
        }
    }
});